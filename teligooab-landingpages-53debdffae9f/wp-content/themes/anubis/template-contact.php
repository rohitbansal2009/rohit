<?php
/* Template Name: Contact */
?>

<?php
	if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
		wpcf7_enqueue_scripts();
		wpcf7_enqueue_styles();
	}
?>

<?php get_header(); ?>

<?php
$options = get_option('anubis'); 
?>

<!-- Start Map -->
<section id="map-area">
	<div class="map" id="map_1" data-mapLat="<?php if(!empty($options['center-lat'])) echo $options['center-lat']; ?>" data-mapLon="<?php if(!empty($options['center-lng'])) echo $options['center-lng']; ?>" data-mapZoom="<?php if(!empty($options['zoom-level'])) echo $options['zoom-level']; ?>" data-mapTitle="<?php if(!empty($options['title-marker'])) echo $options['title-marker']; ?>" data-marker-img="<?php if(!empty($options['marker-img'])) echo $options['marker-img']; ?>" data-info="<?php if(!empty($options['map-info'])) echo $options['map-info']; ?>"></div>
</section>
<!-- End Map -->

<?php if( !empty($options['enable-intro-box']) && $options['enable-intro-box'] == 1) { ?>
<!-- Start Intro Box -->
<section id="intro-box">
	<div class="container">
    	<div class="row">
        	<div class="span12">
                <?php if(!empty($options['intro-box'])) { ?><h3><?php echo $options['intro-box']?></h3><?php } ?>
                <span class="arrow"></span>
            </div>
        </div>
    </div>
</section>
<!-- End Intro Box -->
<?php } ?>

<section id="contact">
    <div id="content">
        <div class="container">
    
            <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
				<?php edit_post_link( __('Edit', AZ_THEME_NAME), '<span class="edit-post">[', ']</span>' ); ?>
                <?php the_content(); ?>
    
            <?php endwhile; endif; ?>
                
        </div>
    </div>
</section>

<?php get_footer(); ?>