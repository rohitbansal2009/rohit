<?php 
/* Template Name: Homepage */

get_header(); ?>
	
<?php az_page_header($post->ID); ?>

<div id="content">
	<div class="container">
		<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
			<?php edit_post_link( __('Edit', AZ_THEME_NAME), '<span class="edit-post">[', ']</span>' ); ?>
            <?php the_content(); ?>
        <?php endwhile; endif; ?>
    </div>
</div>

<!-- Recent Works -->
<section id="latest-work">
	<span class="arrow-down"></span>
    <span class="arrow"></span>
    
    	<div class="container">
        	<div class="row">
                <div class="span12">
                    <h3><?php _e('Latest Works', AZ_THEME_NAME); ?></h3>
                </div>
            </div>
            
            <div class="row">
            	<ul id="latest-work-thumbs">
                
                	<?php
						$span_num = '4'; // Columns Span Value
						
						$posts_per_page = '6'; // Display Limit Post
						
						$portfolio = array(
							'posts_per_page' => $posts_per_page,
							'post_type' => 'portfolio',
							'orderby' => 'menu_order',
							'order' => 'ASC'
						);
				
						$wp_query = new WP_Query($portfolio);
					
						if(have_posts()) : while(have_posts()) : the_post(); ?>
						
						<li class="work-item-thumbs span<?php echo $span_num; ?>">
							<h5>
								<a href="<?php echo get_post_permalink($id); ?>"><?php the_title(); ?></a>
								<span class="arrow-port"></span>
							</h5>
							
                            <?php if( !empty($options['enable-fancybox']) && $options['enable-fancybox'] == 1) { ?>
                                
                                <?php					
                                    $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
                                    $fancy_gallery = get_post_meta($post->ID, '_az_fancy_gallery', true);
                                    $fancy_video = get_post_meta($post->ID, '_az_fancy_video', true);
                                ?>
                                    
                                <?php    
                                    // Video FancyBox
                                    if( !empty($fancy_video)) { ?>
                                    <a class="hover-wrap fancybox-media" href="<?php echo $fancy_video; ?>" title="<?php the_title(); ?>" <?php if(!empty($fancy_gallery)) { ?> data-fancybox-group="<?php echo strtolower($fancy_gallery); ?>" <?php } ?> >
                                        <?php the_post_thumbnail('portfolio-thumb'); ?>
                                        <div class="overlay"></div>
                                        <i class="font-icon-plus"></i>
                                    </a>
                                <?php } 
                                    
                                    // Image FancyBox
                                    else { ?>
                                    <a class="hover-wrap fancybox" href="<?php echo $featured_image[0]; ?>" title="<?php the_title(); ?>" <?php if(!empty($fancy_gallery)) { ?> data-fancybox-group="<?php echo strtolower($fancy_gallery); ?>" <?php } ?> >
                                        <?php the_post_thumbnail('portfolio-thumb'); ?>
                                        <div class="overlay"></div>
                                        <i class="font-icon-plus"></i>
                                    </a>
                                <?php } ?>
                                
                            <?php } 
									// No Fancybox
									else { ?>
									
                                    <a class="hover-wrap" href="<?php echo get_post_permalink($id); ?>" title="<?php the_title(); ?>">
                                        <?php the_post_thumbnail('portfolio-thumb'); ?>
                                        <div class="overlay"></div>
                                        <i class="font-icon-plus"></i>
                                    </a>
                                    
                            <?php } ?>		
						</li>
						
					<?php endwhile; endif; ?>
                
                </ul>
            </div>
        </div>
</section>
<!-- End Recent Works -->

<!-- Recent Posts -->
<section id="latest-blog">
	<div class="container">
		<div class="row">
        	<div class="span12">
            	<h3><?php _e('Latest Posts', AZ_THEME_NAME); ?></h3>
            </div>	
        </div>
        
        <div class="row">
        	<?php 
			    $recentBlogPosts = new WP_Query(array(
			      'showposts' => 3,
			      'ignore_sticky_posts' => 1,
			      'tax_query' => array(
		              array( 'taxonomy' => 'post_format',
		                  'field' => 'slug',
		                  'terms' => array('post-format-link','post-format-quote'),
		                  'operator' => 'NOT IN'
		                  )
		              )
			    ));
				
				if($recentBlogPosts->have_posts()) : while($recentBlogPosts->have_posts()) : $recentBlogPosts->the_post(); ?>
                
        	<article class="post span4">
            	
                <div class="post-thumb">
					<?php 
                        // Video
                        if(get_post_format() == 'video'){
                            $video_embed = get_post_meta($post->ID, '_az_video_embed', true);
        
                            if( !empty( $video_embed ) ) {
                                echo stripslashes(htmlspecialchars_decode($video_embed));
                            } else { 
                                az_video($post->ID); 
                            }
                        }
                        
                        // Audio
                        else if (get_post_format() == 'audio'){
                            az_audio($post->ID); 
                        }
						
						// Gallery
						else if (get_post_format() == 'gallery'){
							$rev_slider_alias_gallery = get_post_meta($post->ID, '_az_gallery', true); 
    	 					echo do_shortcode('[rev_slider '.$rev_slider_alias_gallery.']');	
						}
						
						// Image
						else if (get_post_format() == 'image'){
							if ( (function_exists('has_post_thumbnail')) && (has_post_thumbnail()) ) { 
								$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
								
								<a class="hover-post fancybox" title="<?php the_title(); ?>" href="<?php echo $featured_image[0]; ?>">
									<?php the_post_thumbnail('blog-post-thumb-latest'); ?>
									<div class="overlay"></div>
									<i class="font-icon-plus"></i>
								</a>
						<?php }
						
						} else { 
							
							if ( (function_exists('has_post_thumbnail')) && (has_post_thumbnail()) ) { ?>
							
                            <a class="hover-post" title="<?php the_title(); ?>" href="<?php the_permalink(); ?>">
								<?php the_post_thumbnail('blog-post-thumb-latest'); ?>
                                <div class="overlay"></div>
                                <i class="font-icon-link"></i>
                            </a>
                                
                                
						<?php } 
						
						} ?>
 
            	</div>
                
                <h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> <?php the_title(); ?></a></h2>
				
                <div class="entry-content">
                	<?php the_excerpt(); ?>
                </div>
                
                <div class="entry-meta">
                	<span class="published"><?php the_time( get_option('date_format') ); ?></span>
                	<span class="divider">/</span>
                	<span class="comment-count"><?php comments_popup_link(__('No Comments', AZ_THEME_NAME), __('1 Comment', AZ_THEME_NAME), __('% Comments', AZ_THEME_NAME)); ?></span>
    
    				<?php edit_post_link( __('Edit', AZ_THEME_NAME), ' / <span class="edit-post">', '</span>' ); ?>
                </div>
                
            </article>
            
            <?php endwhile; endif; ?>
            
        </div>
        
	</div>
</section>
<!-- End Recend Post -->
	
<?php get_footer(); ?>