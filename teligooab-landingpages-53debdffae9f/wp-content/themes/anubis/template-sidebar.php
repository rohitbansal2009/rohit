<?php 
/* Template Name: Page Sidebar */

get_header(); ?>

<?php $options = get_option('anubis'); 

$alignment = (!empty($options['sidebar_page_layout'])) ? $options['sidebar_page_layout'] : 'right_side' ;


switch ($alignment) {
	case 'right_side' :
		$align_sidebar = 'right_side';
		$align_main = 'left_side';
		break;
	case 'left_side' :
		$align_sidebar = 'left_side';
		$align_main = 'right_side';
		break;
}
?>


<?php az_page_header($post->ID); ?>


<div id="content">
	<div class="container">
        <div class="row">
        
            <div class="span9 <?php echo $align_main; ?>">
            	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
                	<?php edit_post_link( __('Edit', AZ_THEME_NAME), '<span class="edit-post">[', ']</span>' ); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
            </div>
            
            <div class="span3 <?php echo $align_sidebar; ?>">
            	<aside id="sidebar">
            		<?php get_sidebar(); ?>
                </aside>
            </div>
            
        </div>
    </div>
</div>

<?php get_footer(); ?>