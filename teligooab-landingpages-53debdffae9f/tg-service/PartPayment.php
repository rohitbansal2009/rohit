<!DOCTYPE html>
<html lang="en-US">
<head>
<title>Mobil</title>
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,900' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://localhost/teligooab-landingpages-53debdffae9f/wp-content/themes/anubis/_include/css/fbpopup.css" type="text/css" media="all" />
<script type='text/javascript' src='http://localhost/teligooab-landingpages-53debdffae9f/wp-includes/js/jquery/jquery.js?ver=1.10.2'></script>
<script type='text/javascript' src='http://localhost/teligooab-landingpages-53debdffae9f/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
<link rel='stylesheet' id='contact-form-7-css'  href='http://localhost/teligooab-landingpages-53debdffae9f/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=3.5.1' type='text/css' media='all' />
<link rel='stylesheet' id='rs-settings-css'  href='http://localhost/teligooab-landingpages-53debdffae9f/wp-content/plugins/revslider/rs-plugin/css/settings.css?ver=3.6' type='text/css' media='all' />
<link rel='stylesheet' id='rs-captions-css'  href='http://localhost/teligooab-landingpages-53debdffae9f/wp-content/plugins/revslider/rs-plugin/css/captions.css?ver=3.6' type='text/css' media='all' />
<link rel='stylesheet' id='bootstrap-css'  href='http://localhost/teligooab-landingpages-53debdffae9f/wp-content/themes/anubis/_include/css/bootstrap.min.css?ver=3.6' type='text/css' media='all' />
<link rel='stylesheet' id='main-styles-css'  href='http://localhost/teligooab-landingpages-53debdffae9f/wp-content/themes/anubis/style.css?ver=3.6' type='text/css' media='all' />
<link rel='stylesheet' id='fancybox-css-css'  href='http://localhost/teligooab-landingpages-53debdffae9f/wp-content/themes/anubis/_include/css/fancybox/jquery.fancybox.css?ver=3.6' type='text/css' media='all' />
<link rel='stylesheet' id='font-icon-css'  href='http://localhost/teligooab-landingpages-53debdffae9f/wp-content/themes/anubis/_include/css/fonts.css?ver=3.6' type='text/css' media='all' />
<link rel='stylesheet' id='shortcode-css-css'  href='http://localhost/teligooab-landingpages-53debdffae9f/wp-content/themes/anubis/_include/css/shortcodes.css?ver=3.6' type='text/css' media='all' />
<link rel='stylesheet' id='responsive-css-css'  href='http://localhost/teligooab-landingpages-53debdffae9f/wp-content/themes/anubis/_include/css/responsive.css?ver=3.6' type='text/css' media='all' />
<link rel='stylesheet' id='validationEngine-css'  href='http://localhost/teligooab-landingpages-53debdffae9f/wp-content/themes/anubis/_include/css/validationEngine.jquery.css?ver=3.6' type='text/css' media='all' />
<link rel='stylesheet' id='dynamic_colors-css'  href='http://localhost/teligooab-landingpages-53debdffae9f/wp-content/themes/anubis/_include/css/color.css.php?ver=3.6' type='text/css' media='all' />
<link rel='stylesheet' id='custom_css-css'  href='http://localhost/teligooab-landingpages-53debdffae9f/wp-content/themes/anubis/_include/css/custom.css.php?ver=3.6' type='text/css' media='all' />
</head>
<body>
<div class="tg_main">
<?php
session_start();
include "../config.php";
include_once "../ezSQL/shared/ez_sql_core.php";
include_once "../ezSQL/mysql/ez_sql_mysql.php";

if(!empty($_GET['phone']))
{
	if(!empty($_GET['configid']))
	{
		$configid = $_GET['configid'];
	}
	else{
		$configid = 0;
	}
	echo "<input type=\"hidden\" id=\"phoneid\" value=\"" . $_GET['phone'] . "\">";
	$phoneid = $_GET['phone'];
	$config = new config();
	$db = new ezSQL_mysql($config->db_username,$config->db_password,$config->db_database,$config->db_host);
	
	$phone = $db->get_row("select * from phone where id = " . $phoneid);
	if(!empty($phone)){
		$priceconfig = $db->get_row("select * from phoneconfig where id = " . $configid);
		if(!empty($priceconfig))
		{
			$product_price_with_vat = $priceconfig->price_with_vat;
		}
		else{
			$product_price_with_vat = $phone->price_with_vat;
		}
		$max_partial_payment = round((($product_price_with_vat / 24)+5/2)/5)*5;
?>
<div class="tb_name">
<?php
echo utf8_encode($phone->name);
?>
</div>
<div class="tb_phoneselection">
<?php
$colors = $db->get_results("select * from phonecolor where phoneid = " . $phoneid);
if(!empty($colors))
{
?>
	<div class="tg_left">
			<img width="300px" height="300px" id="imgphcolor" src="<?php echo $colors[0]->image; ?>" >
</div>
<div class="tg_right">
	<?php
	foreach($colors as $color){
		echo "<input type=\"hidden\" id=\"hiddenimg$color->id\" value=\"$color->image\">";
	}
	?><br>
    Välj Färg:</br>
	<select id="phonecolor" name="phonecolor">
	<?php
	unset($color);
	foreach($colors as $color){
		echo "<option value=\"$color->id\">$color->color</option>";
	}
	?>
	</select>
	<br/>
<?php
}
else{
?>
<div class="tg_left">
			<img width="300px" height="300px" id="imgphcolor" src="<?php echo $phone->image; ?>" >
</div>
<div class="tg_right">
	<?php
}
	?>
Välj delbetalning:</br>
<select id="monthly" name="monthly">
<option selected value="1-<?php echo $max_partial_payment ?>"><?php echo $max_partial_payment ?> kr / månad</option></br>
<?php
if ($max_partial_payment - 50 > 0)
{
?>
<option value="2-<?php echo $max_partial_payment - 50 ?>"><?php echo $max_partial_payment - 50 ?> kr / månad</option></br>
<?php
}
if ($max_partial_payment - 100 > 0)
{
?>
<option value="3-<?php echo $max_partial_payment - 100 ?>"><?php echo $max_partial_payment - 100 ?> kr / månad</option>
<?php
}
?>
</select></br>
Välj avbetalningstid:<br/>
<select id="nomonths" name="nomonths">
<option selected value="24">24 månader</option>
<option value="18">18 månader</option>
<option value="12">12 månader</option>
<option value="0">Betala mobilen direkt</option>
</select>
<br/>
<?php
$phoneconfigs = $db->get_results("select * from phoneconfig where phoneid = " . $phoneid);
if(!empty($phoneconfigs))
{
?>
<select id="phoneconfig" name="phoneconfig">
<?php
foreach($phoneconfigs as $phoneconfig){
	if($phoneconfig->id == $configid)
	{
		echo "<option selected value=\"$phoneconfig->id\">$phoneconfig->config</option>";
	}
	else{
		echo "<option value=\"$phoneconfig->id\">$phoneconfig->config</option>";
	}
}
?>
</select>
<?php
}
?>

<br/>
<div class="price-box">
<span id="product-price-795" class="regular-price">
<span class="price">Betalas nu : <label id="firstmonth">0</label> kr</span>
</span>
</div><br/>
<input type="hidden" name="qty" id="qty" value="1" />
<input class="button-main" type="button" name="pay" id="pay" value="Fortsätt" />
</div>
</div>
</div>
<script type="text/javascript">
//<![CDATA[
jQuery(document).ready(function() {

jQuery("#pay").click(function(){
var phonecolor = 0;
var phoneconfig = 0;

if (jQuery('#phonecolor').length > 0 ) {
phonecolor = jQuery("#phonecolor").val();
}

if(jQuery('#phoneconfig').length > 0){
phoneconfig = jQuery("#phoneconfig").val();
}

jQuery.getJSON( "/tg-service/PhoneManager.php", { phn: <?php echo $phoneid; ?>, pay_type: jQuery("#monthly").val().split("-")[0], month: jQuery("#nomonths").val(), color: phonecolor, config: phoneconfig } )
	.done(function( json ) {
if(json.status == 1){
parent.window.location = "/order-page/";
}
else{
parent.jQuery.fancybox.close();
}
	});
});

// Handler for .ready() called.
jQuery("#monthly").change(function(){
if(jQuery(this).val().split("-")[0] == 1 && jQuery("#nomonths").val() == 24){
jQuery("#firstmonth").text("0");
}
else{
			jQuery("#firstmonth").text(<?php echo $product_price_with_vat; ?> - (jQuery(this).val().split("-")[1] * jQuery("#nomonths").val()));
}
}).trigger('change');


jQuery("#phonecolor").change(function(){
var image = jQuery("#hiddenimg" + jQuery(this).val()).val();
jQuery("#imgphcolor").attr("src",image);
});

jQuery("#phoneconfig").change(function(){
window.location = "./PartPayment.php?phone=" + jQuery("#phoneid").val() + "&configid=" + jQuery(this).val();
/*
var price = jQuery("#phoneprice" + jQuery(this).val()).val();
jQuery("#monthly").empty();
jQuery("#monthly").append('<option selected value="1-' + price + '">' + price + '</option>');
if((price - 50) > 0){
jQuery("#monthly").append('<option selected value="2-' + (price - 50) + '">' + (price - 50) + '</option>');
}
if((price - 100) > 0){
jQuery("#monthly").append('<option selected value="3-' + (price - 100) + '">' + (price - 100) + '</option>');
}
*/
});

jQuery("#nomonths").change(function(){
if(jQuery("#monthly").val().split("-")[0] == 1 && jQuery(this).val() == 24){
jQuery("#firstmonth").text("0");
}
else{
			jQuery("#firstmonth").text(<?php echo $product_price_with_vat; ?> - (jQuery("#monthly").val().split("-")[1] * jQuery(this).val()));
}
}).trigger('change');
});
//]]>
</script>
<?php
}
}
?>
</body>
</html>