<?php get_header(); ?>

<?php 
	$page_title = az_custom_get_page_title();

	if( !empty($page_title) ) {
		echo '<section id="title-page"><h2>' . $page_title . '</h2><span class="arrow"></span></section>';
	}
?>	

<div id="content">
	<section id="blog" class="blog-post-full">
        <div class="container">
            <div class="row">
            	<div class="span12">
            
					<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
                    
                    	<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">	
                    
                        	<?php 
							$format = get_post_format(); 
							get_template_part( 'content', $format );
							?>
                        
                        	<div class="separator"></div>
                        </article>
                    
                    <?php endwhile; endif; ?>
                    
                    <?php az_pagination(); ?>
                   
                  </div>  
        	</div> 
        </div>
	</section>
</div>
    
<?php get_footer(); ?>