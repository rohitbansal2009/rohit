<?php
if ( (function_exists('has_post_thumbnail')) && (has_post_thumbnail()) ) { 

$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );

?>

<div class="post-thumb">
    <a class="hover-post fancybox" title="<?php the_title(); ?>" href="<?php echo $featured_image[0]; ?>">
        <?php the_post_thumbnail('blog-post-thumb'); ?>
        <div class="overlay"></div>
        <i class="font-icon-plus"></i>
    </a>
</div>

<?php } ?>

<?php if( !is_single() ) { ?>
<div class="entry-type">
    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><i class="font-icon-expand-view"></i></a>
</div>

<div class="row">
	<div class="span6 offset3">
    	<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> <?php the_title(); ?></a></h2>
        
        <?php get_template_part( 'content' , 'meta-header' ); ?>
        
        <div class="entry-content">
			<?php the_content( __("Continue Reading...", AZ_THEME_NAME) );?>
		</div>
        
    </div>
</div>

<?php } else { ?>

<div class="separator"></div>

<div class="entry-content">
    <?php the_content( __("Continue Reading...", AZ_THEME_NAME) );?>
</div>

<?php get_template_part( 'content' , 'meta-footer' ); ?>

<?php } ?>
