<?php 
/* Template Name: Portfolio */

get_header(); 

$options = get_option('anubis'); 

$cols = (!empty($options['main_portfolio_layout'])) ? $options['main_portfolio_layout'] : '4' ;

switch ($cols) {
	case '4' :
		$span_num = '3';
		break;
	case '3' :
		$span_num = '4';
		break;
	case '2' :
		$span_num = '6';
		break;
}
?>

<?php az_page_header($post->ID); ?>

<div id="content">
	<div class="container">
    
    	<section id="portfolio-filter" class="margin-50">
        	<div class="row">
            	<div class="span12">
                
                	<div class="dropdown">
                        <div class="dropmenu">
                            <p class="selected"><?php _e('All Projects', AZ_THEME_NAME); ?></p>
                            <i class="font-icon-arrow-simple-down"></i>
                        </div>
            
                        <div class="dropmenu-active">
                            <ul class="option-set" data-option-key="filter">
                                <li><a class="selected drop-selected" href="#filter" data-option-value="*"><?php _e('All Projects', AZ_THEME_NAME); ?></a></li>
                                
                                <?php $list_categories = get_categories("taxonomy=project-category");
									foreach ($list_categories as $list_category) : 									
									echo '<li><a href="#filter" data-option-value=".' . strtolower(str_replace(" ","-", ($list_category->name))) . '">' . $list_category->name . '</a></li>';
									endforeach;        
								?>

                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        
        <section id="portfolio">
        	<div class="row">               
                	<div id="portfolio-projects">
                		<ul id="projects">
                        	
                            <?php
								$posts_per_page = '-1';
								$portfolio = array(
									'posts_per_page' => $posts_per_page,
									'post_type' => 'portfolio',
									'orderby' => 'menu_order',
									'order' => 'ASC'
								);
		
								$wp_query = new WP_Query($portfolio);
							
								if(have_posts()) : while(have_posts()) : the_post(); ?>
                                
                                <?php					
									$terms = get_the_terms($post->id,"project-category");
									$list_categories = NULL;
									
										if ( !empty($terms) ){
										 foreach ( $terms as $term ) {
										   $list_categories .= strtolower($term->slug) . ' ';
										 }
									}
								?>
                                
                                <li class="item-project span<?php echo $span_num; ?> <?php echo $list_categories; ?>">
                                    <h5>
                                        <a href="<?php echo get_post_permalink($id); ?>"><?php the_title(); ?></a>
                                        <span class="arrow-port"></span>
                                    </h5>
                                    
                                    <?php if( !empty($options['enable-fancybox']) && $options['enable-fancybox'] == 1) { ?>
                                    
										<?php					
                                            $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
                                            $fancy_gallery = get_post_meta($post->ID, '_az_fancy_gallery', true);
                                            $fancy_video = get_post_meta($post->ID, '_az_fancy_video', true);
                                        ?>
                                            
                                        <?php    
                                            // Video FancyBox
                                            if( !empty($fancy_video)) { ?>
                                            <a class="hover-wrap fancybox-media" href="<?php echo $fancy_video; ?>" title="<?php the_title(); ?>" <?php if(!empty($fancy_gallery)) { ?> data-fancybox-group="<?php echo strtolower($fancy_gallery); ?>" <?php } ?> >
                                                <?php the_post_thumbnail('portfolio-thumb'); ?>
                                                <div class="overlay"></div>
                                                <i class="font-icon-plus"></i>
                                            </a>
                                        <?php } 
                                            
                                            // Image FancyBox
                                            else { ?>
                                            <a class="hover-wrap fancybox" href="<?php echo $featured_image[0]; ?>" title="<?php the_title(); ?>" <?php if(!empty($fancy_gallery)) { ?> data-fancybox-group="<?php echo strtolower($fancy_gallery); ?>" <?php } ?> >
                                                <?php the_post_thumbnail('portfolio-thumb'); ?>
                                                <div class="overlay"></div>
                                                <i class="font-icon-plus"></i>
                                            </a>
                                        <?php }?>
                                    
                                    <?php } 
											// No FancyBox 
											else { ?>
							                  
                                            <a class="hover-wrap" href="<?php echo get_post_permalink($id); ?>" title="<?php the_title(); ?>">
                                                <?php the_post_thumbnail('portfolio-thumb'); ?>
                                                <div class="overlay"></div>
                                                <i class="font-icon-plus"></i>
                                            </a>
                                                   
                                    <?php } ?>         
                                </li>
                                
                            <?php endwhile; endif; ?>
                            
                        </ul>
                    </div>

            </div>
        </section>
        
    </div>
</div>

<?php get_footer(); ?>