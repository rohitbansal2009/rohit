<?php get_header(); ?>

<div id="content">
    <section id="error-404">
        <div class="container">
            <div class="row">
                <div class="span12">		
                    <h1>404</h1>
                    <h2><?php echo __('Not Found', AZ_THEME_NAME); ?></h2>
                </div>
            </div>
        </div>
    </section>
</div>
	
<?php get_footer(); ?>

