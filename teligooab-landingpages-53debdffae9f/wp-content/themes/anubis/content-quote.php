<?php $quote = get_post_meta($post->ID, '_az_quote', true); ?>

<?php if( !is_single() ) { ?>

<div class="entry-type">
    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><i class="font-icon-blockquote"></i></a>
</div>

<div class="row">
	<div class="span6 offset3">
    	        
        <h2 class="entry-title quote-text"><?php echo $quote; ?></h2>
        
    </div>
</div>

<?php } else { ?>

<h2 class="entry-title quote-text text-align-center"><?php echo $quote; ?></h2>
<div class="separator"></div>

<div class="entry-content">
    <?php the_content( __("Continue Reading...", AZ_THEME_NAME) );?>
</div>

<?php get_template_part( 'content' , 'meta-footer' ); ?>

<?php } ?>