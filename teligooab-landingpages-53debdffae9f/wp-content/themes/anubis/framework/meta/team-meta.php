<?php

/*-----------------------------------------------------------------------------------*/
/*	Team Meta
/*-----------------------------------------------------------------------------------*/

add_action('add_meta_boxes', 'az_metabox_team');
function az_metabox_team(){
    
	
/*-----------------------------------------------------------------------------------*/
/*	Social Profile Setting
/*-----------------------------------------------------------------------------------*/

	$meta_box = array(
		'id' => 'az-metabox-social-profile',
		'title' =>  __('Social Profile', AZ_THEME_NAME),
		'description' => __('If you want you can insert for each people a unique social profile URL.', AZ_THEME_NAME),
		'post_type' => 'team',
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
    		array( 
				'name' => __('500PX Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your 500PX URL.', AZ_THEME_NAME),
				'id' => '_az_profile_500px',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Add This Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Add This URL.', AZ_THEME_NAME),
				'id' => '_az_profile_addthis',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Behance Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Behance URL.', AZ_THEME_NAME),
				'id' => '_az_profile_behance',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Bebo Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Bebo URL.', AZ_THEME_NAME),
				'id' => '_az_profile_bebo',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Blogger Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Blogger URL.', AZ_THEME_NAME),
				'id' => '_az_profile_blogger',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Deviant Art Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Deviant Art URL.', AZ_THEME_NAME),
				'id' => '_az_profile_deviant-art',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Digg Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Digg URL.', AZ_THEME_NAME),
				'id' => '_az_profile_digg',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Dribbble Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Dribbble URL.', AZ_THEME_NAME),
				'id' => '_az_profile_dribbble',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Email Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Email URL.', AZ_THEME_NAME),
				'id' => '_az_profile_email',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Envato Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Envato URL.', AZ_THEME_NAME),
				'id' => '_az_profile_envato',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Evernote Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Evernote URL.', AZ_THEME_NAME),
				'id' => '_az_profile_evernote',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Facebook Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Facebook URL.', AZ_THEME_NAME),
				'id' => '_az_profile_facebook',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Flickr Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Flickr URL.', AZ_THEME_NAME),
				'id' => '_az_profile_flickr',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Forrst Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Forrst URL.', AZ_THEME_NAME),
				'id' => '_az_profile_forrst',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Github Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Github URL.', AZ_THEME_NAME),
				'id' => '_az_profile_github',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Google Plus Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Google Plus URL.', AZ_THEME_NAME),
				'id' => '_az_profile_google-plus',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Grooveshark Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Grooveshark URL.', AZ_THEME_NAME),
				'id' => '_az_profile_grooveshark',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Instagram Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Instagram URL.', AZ_THEME_NAME),
				'id' => '_az_profile_instagram',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Last FM Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Last FM URL.', AZ_THEME_NAME),
				'id' => '_az_profile_last-fm',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Linked In Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Linked In URL.', AZ_THEME_NAME),
				'id' => '_az_profile_linkedin',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('My Space Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your My Space URL.', AZ_THEME_NAME),
				'id' => '_az_profile_myspace',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('PayPal Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Paypal URL.', AZ_THEME_NAME),
				'id' => '_az_profile_paypal',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Photobucket Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Photobucket URL.', AZ_THEME_NAME),
				'id' => '_az_profile_photobucket',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Pinterest Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Pinterest URL.', AZ_THEME_NAME),
				'id' => '_az_profile_pinterest',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Quora Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Quora URL.', AZ_THEME_NAME),
				'id' => '_az_profile_quora',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Share This Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Share This URL.', AZ_THEME_NAME),
				'id' => '_az_profile_share-this',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Skype Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Skype URL.', AZ_THEME_NAME),
				'id' => '_az_profile_skype',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Soundcloud Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Soundcloud URL.', AZ_THEME_NAME),
				'id' => '_az_profile_soundcloud',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Stumble Upon Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Stumble Upon URL.', AZ_THEME_NAME),
				'id' => '_az_profile_stumbleupon',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Tumblr Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Tumblr URL.', AZ_THEME_NAME),
				'id' => '_az_profile_tumblr',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Twitter Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Twitter URL.', AZ_THEME_NAME),
				'id' => '_az_profile_twitter',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Viddler Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Viddler URL.', AZ_THEME_NAME),
				'id' => '_az_profile_viddler',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Vimeo Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Vimeo URL.', AZ_THEME_NAME),
				'id' => '_az_profile_vimeo',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Virb Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Virb URL.', AZ_THEME_NAME),
				'id' => '_az_profile_virb',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Wordpress Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Wordpress URL.', AZ_THEME_NAME),
				'id' => '_az_profile_wordpress',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Yahoo Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Yahoo URL.', AZ_THEME_NAME),
				'id' => '_az_profile_yahoo',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Yelp Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Yelp URL.', AZ_THEME_NAME),
				'id' => '_az_profile_yelp',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('You Tube Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your You Tube URL.', AZ_THEME_NAME),
				'id' => '_az_profile_youtube',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('Zerply Profile', AZ_THEME_NAME),
				'desc' => __('Please enter in your Zerply URL.', AZ_THEME_NAME),
				'id' => '_az_profile_zerply',
				'type' => 'text',
				'std' => ''
			)
		)
	);
	
	$callback = create_function( '$post,$meta_box', 'az_create_meta_box( $post, $meta_box["args"] );' );
	add_meta_box( $meta_box['id'], $meta_box['title'], $callback, $meta_box['post_type'], $meta_box['context'], $meta_box['priority'], $meta_box );

}