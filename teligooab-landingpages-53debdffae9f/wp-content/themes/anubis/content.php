<?php if( !is_single() ) { ?>

<?php  if ( (function_exists('has_post_thumbnail')) && (has_post_thumbnail()) ) { ?>

<div class="post-thumb">
    <a class="hover-post" title="<?php the_title(); ?>" href="<?php the_permalink(); ?>">
        <?php the_post_thumbnail('blog-post-thumb'); ?>
        <div class="overlay"></div>
        <i class="font-icon-link"></i>
    </a>
</div>

<?php } ?>

<div class="entry-type">
    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><i class="font-icon-align-center"></i></a>
</div>

<div class="row">
	<div class="span6 offset3">
    	<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> <?php the_title(); ?></a></h2>
        
        <?php get_template_part( 'content' , 'meta-header' ); ?>
        
        <div class="entry-content">
			<?php the_content( __("Continue Reading...", AZ_THEME_NAME) );?>
		</div>
        
    </div>
</div>

<?php } else { ?>

<div class="post-thumb">
	<?php the_post_thumbnail('blog-post-thumb'); ?>
</div>
    
<div class="entry-content">
    <?php the_content( __("Continue Reading...", AZ_THEME_NAME) );?>
</div>

<?php get_template_part( 'content' , 'meta-footer' ); ?>


<?php } ?>
