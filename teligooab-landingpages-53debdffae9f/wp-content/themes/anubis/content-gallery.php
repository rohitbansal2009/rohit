<div class="post-thumb">

	<?php $rev_slider_alias_gallery = get_post_meta($post->ID, '_az_gallery', true); ?>
    
    <?php echo do_shortcode('[rev_slider '.$rev_slider_alias_gallery.']'); ?>
	
</div>

<?php if( !is_single() ) { ?>
<div class="entry-type">
    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><i class="font-icon-grid-large"></i></a>
</div>

<div class="row">
	<div class="span6 offset3">
    	<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> <?php the_title(); ?></a></h2>
        
        <?php get_template_part( 'content' , 'meta-header' ); ?>
        
        <div class="entry-content">
			<?php the_content( __("Continue Reading...", AZ_THEME_NAME) );?>
		</div>
        
    </div>
</div>

<?php } else { ?>

<div class="separator"></div>

<div class="entry-content">
    <?php the_content( __("Continue Reading...", AZ_THEME_NAME) );?>
</div>

<?php get_template_part( 'content' , 'meta-footer' ); ?>

<?php } ?>