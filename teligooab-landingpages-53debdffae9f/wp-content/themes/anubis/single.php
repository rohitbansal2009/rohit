<?php get_header(); ?>

<?php
$options = get_option('anubis'); 

$alignment = (!empty($options['sidebar_post_layout'])) ? $options['sidebar_post_layout'] : 'right_side' ;

switch ($alignment) {
	case 'right_side' :
		$align_sidebar = 'right_side';
		$align_main = 'left_side';
		break;
	case 'left_side' :
		$align_sidebar = 'left_side';
		$align_main = 'right_side';
		break;
}
?>

<?php az_post_header($post->ID); ?>

<div id="content">
	<section id="blog" class="single-post">
        <div class="container">
            <div class="row">
            
            	<div class="span9 <?php echo $align_main; ?>">
            
					<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
                    
                    	<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">	
                    
                        	<?php 
							$format = get_post_format(); 
							get_template_part( 'content', $format );
							?>
                        
                        	<div class="separator"></div>
                        </article>
                        
                        <?php comments_template('', true); ?>
                    
                    <?php endwhile; endif; ?>
              
				</div>  
                
                <div class="span3 <?php echo $align_sidebar; ?>">
                	<aside id="sidebar">
                		<?php get_sidebar(); ?>
                    </aside>
                </div>
        	</div> 
        </div>
	</section>
</div>

<?php get_footer(); ?>