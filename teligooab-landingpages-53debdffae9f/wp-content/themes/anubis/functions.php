<?php 

/*-----------------------------------------------------------------------------------

	Here we have all the custom functions for the theme
	Please be extremely cautious editing this file,
	When things go wrong, they tend to go wrong in a big way.
	You have been warned!

-------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------*/
/*  Set Max Content Width
/* ----------------------------------------------------------------------------------*/
if ( ! isset( $content_width ) )
	$content_width = 1170;

/*-----------------------------------------------------------------------------------*/
/*	Default Theme Constant
/*-----------------------------------------------------------------------------------*/

define('AZ_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/framework/');
define('AZ_THEME_NAME', 'anubis');


/*-----------------------------------------------------------------------------------*/
/*	Language
/*-----------------------------------------------------------------------------------*/

function az_lang_setup(){
	load_theme_textdomain(AZ_THEME_NAME, get_template_directory() . '/languages');
}
add_action('after_setup_theme', 'az_lang_setup');

/*-----------------------------------------------------------------------------------*/
/*	Remove Generator for Security
/*-----------------------------------------------------------------------------------*/

remove_action( 'wp_head', 'wp_generator' );

/*-----------------------------------------------------------------------------------*/
/*	Custom Login Logo
/*-----------------------------------------------------------------------------------*/

if ( !function_exists( 'az_custom_login_logo' ) ) {
	function az_custom_login_logo() {
	    echo '<style type="text/css">
	        h1 a { background-image:url('.get_template_directory_uri().'/_include/img/logo-admin.png) !important; height: 98px !important; background-size: auto auto !important; }
	    </style>';
	}
}
add_action('login_head', 'az_custom_login_logo');

if ( !function_exists( 'az_wp_login_url' ) ) {
	function az_wp_login_url() {
		return home_url();
	}
}
add_filter('login_headerurl', 'az_wp_login_url');

if ( !function_exists( 'az_wp_login_title' ) ) {
	function az_wp_login_title() {
		return get_option('blogname');
	}
}
add_filter('login_headertitle', 'az_wp_login_title');


/*-----------------------------------------------------------------------------------*/
/*	Register / Enqueue JS
/*-----------------------------------------------------------------------------------*/

function az_register_js() {	
	
	if (!is_admin()) {
		
		// Register 
		wp_register_script('modernizer', get_template_directory_uri() . '/_include/js/modernizr.js', 'jquery', '2.5.3');
		wp_register_script('bootstrap-js', get_template_directory_uri() . '/_include/js/bootstrap.min.js', 'jquery', '2.3', TRUE);
		wp_register_script('superfish', get_template_directory_uri() . '/_include/js/jquery.superfish.min.js', 'jquery', '1.7.2', TRUE);
		wp_register_script('supersubs', get_template_directory_uri() . '/_include/js/jquery.supersubs.min.js', 'jquery', '0.3', TRUE);
		wp_register_script('isotope', get_template_directory_uri() . '/_include/js/jquery.isotope.js', 'jquery', '1.5.25' ,TRUE);
		wp_register_script('fancybox-js', get_template_directory_uri() . '/_include/js/jquery.fancybox.pack.js', 'jquery', '2.1.4' ,TRUE);
		wp_register_script('fancybox-js-media', get_template_directory_uri() . '/_include/js/jquery.fancybox-media.js', 'jquery', '1.0.5' ,TRUE);
		wp_register_script('jplayer', get_template_directory_uri() . '/_include/js/jplayer.min.js', 'jquery', '2.3.0', TRUE);
		wp_register_script('fitvids', get_template_directory_uri() . '/_include/js/jquery.fitvids.js', 'jquery', '1.0' ,TRUE);
		wp_register_script('plugins', get_template_directory_uri() . '/_include/js/plugins.js', 'jquery', '1.0.0' ,TRUE);
		wp_register_script('main', get_template_directory_uri() . '/_include/js/main.js', array('jquery', 'bootstrap-js', 'superfish', 'supersubs', 'isotope', 'fancybox-js', 'fancybox-js-media', 'fitvids', 'plugins'), '1.0', TRUE);
		
		// Enqueue
		wp_enqueue_script('jquery');
		wp_enqueue_script('modernizer');
		wp_enqueue_script('bootstrap-js'); 
		wp_enqueue_script('superfish'); 
		wp_enqueue_script('supersubs');
		wp_enqueue_script('isotope');
		wp_enqueue_script('fancybox-js');
		wp_enqueue_script('fancybox-js-media');
		wp_enqueue_script('jplayer');
		wp_enqueue_script('fitvids'); 
		wp_enqueue_script('plugins');
		wp_enqueue_script('main');
	}
}

add_action('wp_enqueue_scripts', 'az_register_js');


function az_page_specific_js() {
	
	// Contact Page
	if ( is_page_template('template-contact.php') ) {
		wp_register_script('googleMaps', 'http://maps.google.com/maps/api/js?sensor=false', NULL, NULL, TRUE);
		wp_register_script('azMap', get_template_directory_uri() . '/_include/js/map.js', array('jquery', 'googleMaps'), '1.0', TRUE);
		
		wp_enqueue_script('googleMaps');
		wp_enqueue_script('azMap');
	}
	
	if( is_singular() ) wp_enqueue_script( 'comment-reply' );
}

add_action('wp_enqueue_scripts', 'az_page_specific_js'); 



/*-----------------------------------------------------------------------------------*/
/*	Register / Enqueue CSS
/*-----------------------------------------------------------------------------------*/

//Main Styles
function az_main_styles() {	
		 
		 // Register 
		 wp_register_style('bootstrap', get_template_directory_uri() . '/_include/css/bootstrap.min.css');
		 wp_register_style("main-styles", get_stylesheet_directory_uri() . "/style.css");
		 wp_register_style("fancybox-css", get_stylesheet_directory_uri() . "/_include/css/fancybox/jquery.fancybox.css");
		 wp_register_style("font-icon", get_stylesheet_directory_uri() . "/_include/css/fonts.css");
		 wp_register_style("shortcode-css", get_stylesheet_directory_uri() . "/_include/css/shortcodes.css");
		 wp_register_style("responsive-css", get_stylesheet_directory_uri() . "/_include/css/responsive.css");
		 
		 
		 // Enqueue
		 wp_enqueue_style('bootstrap'); 
		 wp_enqueue_style('main-styles');
		 wp_enqueue_style('fancybox-css'); 
		 wp_enqueue_style('font-icon'); 
		 wp_enqueue_style('shortcode-css');
		 wp_enqueue_style('responsive-css'); 
}

add_action('wp_print_styles', 'az_main_styles');



/*-----------------------------------------------------------------------------------*/
/*	Dynamic Styles
/*-----------------------------------------------------------------------------------*/

function az_enqueue_dynamic_css() {
	wp_register_style('dynamic_colors', get_stylesheet_directory_uri() . '/_include/css/color.css.php', 'style');
	wp_enqueue_style('dynamic_colors'); 
	
	wp_register_style('custom_css', get_stylesheet_directory_uri() . '/_include/css/custom.css.php', 'style');
	wp_enqueue_style('custom_css');
} 

add_action('wp_print_styles', 'az_enqueue_dynamic_css');



/*-----------------------------------------------------------------------------------*/
/*	Custom Menu
/*-----------------------------------------------------------------------------------*/

if ( function_exists( 'register_nav_menus' ) ) {
	register_nav_menus(array('primary_menu' => __('Primary Menu', AZ_THEME_NAME) ));
}	



/*-----------------------------------------------------------------------------------*/
/*	Widget Area
/*-----------------------------------------------------------------------------------*/

if(function_exists('register_sidebar')) {
	
	register_sidebar(array(
		'name' => __('Blog Sidebar', AZ_THEME_NAME), 
		'description' => __('Widget area for blog pages.', AZ_THEME_NAME),
		'id' => 'sidebar-blog',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  =>
		'<h3 class="widget-title">',
		'after_title'   => '</h3>'
		)
	);
	
	register_sidebar(array(
		'name' => __('Page Sidebar', AZ_THEME_NAME), 
		'description' => __('Widget area for pages.', AZ_THEME_NAME),
		'id' => 'sidebar-page', 
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>', 
		'before_title'  => '<h3 class="widget-title">', 
		'after_title'   => '</h3>'
		)
	);
	
	register_sidebar(array(
		'name' => __('Footer Area 1', AZ_THEME_NAME), 
		'description' => __('Widget area for footer area.', AZ_THEME_NAME),
		'id' => 'footer-area-one', 
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>', 
		'before_title'  => '<h3>', 
		'after_title'   => '</h3>'
		)
	);
	
	register_sidebar(array(
		'name' => __('Footer Area 2', AZ_THEME_NAME), 
		'description' => __('Widget area for footer area.', AZ_THEME_NAME),
		'id' => 'footer-area-two',  
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>', 
		'before_title'  => '<h3>', 
		'after_title'   => '</h3>'
		)
	);
	
	register_sidebar(array(
		'name' => __('Footer Area 3', AZ_THEME_NAME), 
		'description' => __('Widget area for footer area.', AZ_THEME_NAME),
		'id' => 'footer-area-three',  
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>', 
		'before_title'  => '<h3>', 
		'after_title'   => '</h3>'
		)
	);
}



/*------------------------------------------------------------------------------*/
/*	Widgets: Twitter - Flickr
/*------------------------------------------------------------------------------*/

include('framework/widgets/twitter-widget.php');
include('framework/widgets/flickr-widget.php');
include('framework/widgets/social-widget.php');


/*-----------------------------------------------------------------------------------*/
/*	Post Formats
/*-----------------------------------------------------------------------------------*/

add_theme_support( 'post-formats', array('quote','video','audio', 'image', 'gallery','link') );



/*-----------------------------------------------------------------------------------*/
/*	Automatic Feed Links
/*-----------------------------------------------------------------------------------*/

if(function_exists('add_theme_support')) {
    add_theme_support('automatic-feed-links');
}



/*-----------------------------------------------------------------------------------*/
/*	Post Thumbnails
/*-----------------------------------------------------------------------------------*/

add_theme_support( 'post-thumbnails' );
add_image_size( 'portfolio-thumb', 600, 450, true ); 
add_image_size( 'blog-post-thumb', 1170, 500, true ); 
add_image_size( 'blog-post-thumb-latest', 600, 400, true );


/*-----------------------------------------------------------------------------------*/
/*	Shortcodes
/*-----------------------------------------------------------------------------------*/

//TinyMCE button + generator
require_once ( 'framework/tinymce/tinymce-class.php' );	


//Shortcode Processing
require_once ( 'framework/tinymce/shortcode-processing.php' );


/*-----------------------------------------------------------------------------------*/
/*	Common Fix
/*-----------------------------------------------------------------------------------*/

//Fixing filtering for shortcodes
function shortcode_empty_paragraph_fix($content){   
    $array = array (
        '<p>[' => '[', 
        ']</p>' => ']', 
        ']<br />' => ']'
    );

    $content = strtr($content, $array);
    return $content;
}

add_filter('the_content', 'shortcode_empty_paragraph_fix');


// Remove rel attribute from the category list
function remove_category_list_rel( $output ) {
    return str_replace( ' rel="category tag"', '', $output );
}
 
add_filter( 'wp_list_categories', 'remove_category_list_rel' );
add_filter( 'the_category', 'remove_category_list_rel' );

// Twitter Filter
function TwitterFilter($string)
{
$content_array = explode(" ", $string);
$output = '';

foreach($content_array as $content)
{
//starts with http://
if(substr($content, 0, 7) == "http://")
$content = '<a href="' . $content . '">' . $content . '</a>';

//starts with www.
if(substr($content, 0, 4) == "www.")
$content = '<a href="http://' . $content . '">' . $content . '</a>';

if(substr($content, 0, 8) == "https://")
$content = '<a href="' . $content . '">' . $content . '</a>';

if(substr($content, 0, 1) == "#")
$content = '<a href="https://twitter.com/search?src=hash&q=' . $content . '">' . $content . '</a>';

if(substr($content, 0, 1) == "@")
$content = '<a href="https://twitter.com/' . $content . '">' . $content . '</a>';

$output .= " " . $content;
}

$output = trim($output);
return $output;
}

function attr($s,$attrname) { // return html attribute
	preg_match_all('#\s*('.$attrname.')\s*=\s*["|\']([^"\']*)["|\']\s*#i', $s, $x);
	if (count($x)>=3) return $x[2][0]; else return "";
}




/*-----------------------------------------------------------------------------------*/
/* Exclude Pages from search
/*-----------------------------------------------------------------------------------*/

if( !function_exists( 'az_exclude_pages_in_search' ) ) {
    function az_exclude_pages_in_search($query) {
        if( $query->is_search ) {
            $query->set('post_type', 'post');
        }
    return $query;
    }
}

add_filter('pre_get_posts','az_exclude_pages_in_search');



/*-----------------------------------------------------------------------------------*/
/*	Excerpt Length
/*-----------------------------------------------------------------------------------*/

if ( !function_exists( 'az_excerpt_length' ) ) {
	function az_excerpt_length($length) {
		return 35; 
	}
}
add_filter('excerpt_length', 'az_excerpt_length');



if ( !function_exists( 'az_excerpt_more' ) ) {
	function az_excerpt_more($excerpt) {
		return str_replace('[...]', '...', $excerpt); 
	}
}
add_filter('wp_trim_excerpt', 'az_excerpt_more');


/*-----------------------------------------------------------------------------------*/
/*	Meta Config
/*-----------------------------------------------------------------------------------*/

function enqueue_media(){
	
	//enqueue the correct media scripts for the media library 
	$wp_version = floatval(get_bloginfo('version'));
	
	if ( $wp_version < "3.5" ) {
	    wp_enqueue_script(
	        'redux-opts-field-upload-js', 
	        Redux_OPTIONS_URL . 'fields/upload/field_upload_3_4.js', 
	        array('jquery', 'thickbox', 'media-upload'),
	        time(),
	        true
	    );
	    wp_enqueue_style('thickbox');// thanks to https://github.com/rzepak
	} else {
	    wp_enqueue_script(
	        'redux-opts-field-upload-js', 
	        Redux_OPTIONS_URL . 'fields/upload/field_upload.js', 
	        array('jquery'),
	        time(),
	        true
	    );
	    wp_enqueue_media();
	}
	
}


//Meta Styling
function  az_metabox_styles() {
	wp_enqueue_style('az_meta_css', AZ_FRAMEWORK_DIRECTORY .'/assets/css/az_meta.css');
}


//Meta Scripts
function az_metabox_scripts() {
	wp_register_script('az-upload', AZ_FRAMEWORK_DIRECTORY .'/assets/js/az-meta.js', array('jquery','media-upload','thickbox'));
	wp_enqueue_script('az-upload');
	wp_localize_script('redux-opts-field-upload-js', 'redux_upload', array('url' => Redux_OPTIONS_URL .'fields/upload/blank.png'));
}

add_action('admin_enqueue_scripts', 'az_metabox_scripts');
add_action('admin_print_styles', 'az_metabox_styles');
add_action('admin_print_styles', 'enqueue_media');


//Meta Core functions
include("framework/meta/meta-config.php");



/*-----------------------------------------------------------------------------------*/
/*	Page Meta Header
/*-----------------------------------------------------------------------------------*/

include("framework/meta/page-meta.php");

if ( !function_exists( 'az_page_header' ) ) {
    function az_page_header($postid) {
		
		global $options;
		global $post;
		
    	$bg = get_post_meta($postid, '_az_header_bg', true);
		$intro = get_post_meta($postid, '_az_intro_box_header', true);
		$title_page = get_post_meta($postid, '_az_title_page', true);
		$rev_slider_alias = get_post_meta($post->ID, '_az_intro_slider_header', true);
			
		if( !empty($bg) ) { 
    ?>	
        <section id="image-static">
            <div class="fullimage-container">
                <div class="pattern"></div>
                <img src="<?php echo $bg; ?>" alt="<?php the_title(); ?>" />
            </div>
        </section>
        
			<?php if( !empty($intro) ) { ?>
            <section id="intro-box">
                <div class="container">
                    <div class="row">
                        <div class="span12">
                            <h3><?php echo $intro; ?></h3>
                            <span class="arrow"></span>
                        </div>
                    </div>
                </div>
            </section>
            
            <?php } ?>
	
	    <?php } else if( !empty($rev_slider_alias) ) { ?>
        	<section id="slider-header">
	    		<?php echo do_shortcode('[rev_slider '.$rev_slider_alias.']'); ?>
	    	</section>
            
            <?php if( !empty($intro) ) { ?>
            <section id="intro-box">
                <div class="container">
                    <div class="row">
                        <div class="span12">
                            <h3><?php echo $intro; ?></h3>
                            <span class="arrow"></span>
                        </div>
                    </div>
                </div>
            </section>
            
            <?php } ?>
            
		<?php } else { ?>
			
        <section id="title-page">
            <h2><?php echo the_title(); ?></h2>
            <span class="arrow"></span>
        </section>
        
		<?php }
    }
}



/*-----------------------------------------------------------------------------------*/
/*	Team Taxanomy / Team Meta / Get Link Back
/*-----------------------------------------------------------------------------------*/

// Team Meta
include("framework/meta/team-meta.php");


// Get Link Back
function get_team_page_link($post_id) {
    global $wpdb;
	
    $results = $wpdb->get_results("SELECT post_id FROM $wpdb->postmeta
    WHERE meta_key='_wp_page_template' AND meta_value='template-team.php'");

    foreach ($results as $result) 
    {
        $page_id = $result->post_id;
    }
	
    return get_page_link($page_id);
}


// Taxanomy Register
function team_register() {  
    	 
	 $team_labels = array(
	 	'name' => __( 'Team', 'taxonomy general name', AZ_THEME_NAME),
		'singular_name' => __( 'People Item', AZ_THEME_NAME),
		'search_items' =>  __( 'Search People Item', AZ_THEME_NAME),
		'all_items' => __( 'Team', AZ_THEME_NAME),
		'parent_item' => __( 'Parent People Item', AZ_THEME_NAME),
		'edit_item' => __( 'Edit People Item', AZ_THEME_NAME),
		'update_item' => __( 'Update People Item', AZ_THEME_NAME),
		'add_new_item' => __( 'Add New People Item', AZ_THEME_NAME)
	 );
	 
	 $options = get_option('anubis'); 
     $custom_slug = null;		
	 
	 if(!empty($options['team_rewrite_slug'])) $custom_slug = $options['team_rewrite_slug'];
	
	 $args = array(
			'labels' => $team_labels,
			'rewrite' => array('slug' => $custom_slug,'with_front' => false),
			'singular_label' => __('Person', AZ_THEME_NAME),
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'hierarchical' => false,
			'menu_position' => 9,
			'menu_icon' => AZ_FRAMEWORK_DIRECTORY . 'assets/img/icons/portfolio.png',
			'supports' => array('title', 'editor', 'thumbnail')  
       );  
  
    register_post_type( 'team' , $args );  
}  
add_action('init', 'team_register');

$category_labels = array(
	'name' => __( 'Disciplines', AZ_THEME_NAME),
	'singular_name' => __( 'Discipline', AZ_THEME_NAME),
	'search_items' =>  __( 'Search Discipline', AZ_THEME_NAME),
	'all_items' => __( 'All Discipline', AZ_THEME_NAME),
	'parent_item' => __( 'Parent Discipline', AZ_THEME_NAME),
	'edit_item' => __( 'Edit Discipline', AZ_THEME_NAME),
	'update_item' => __( 'Update Discipline', AZ_THEME_NAME),
	'add_new_item' => __( 'Add New Discipline', AZ_THEME_NAME),
    'menu_name' => __( 'Disciplines', AZ_THEME_NAME)
); 	

register_taxonomy("disciplines", 
		array("team"), 
		array("hierarchical" => true, 
				'labels' => $category_labels,
				'show_ui' => true,
    			'query_var' => true,
				'rewrite' => array( 'slug' => 'disciplines' )
));

$attributes_labels = array(
	'name' => __( 'Attributes', AZ_THEME_NAME),
	'singular_name' => __( 'Attribute', AZ_THEME_NAME),
	'search_items' =>  __( 'Search Attributes', AZ_THEME_NAME),
	'all_items' => __( 'All Attributes', AZ_THEME_NAME),
	'parent_item' => __( 'Parent Attribute', AZ_THEME_NAME),
	'edit_item' => __( 'Edit Attribute', AZ_THEME_NAME),
	'update_item' => __( 'Update Attribute', AZ_THEME_NAME),
	'add_new_item' => __( 'Add New Attribute', AZ_THEME_NAME),
	'new_item_name' => __( 'New Attribute', AZ_THEME_NAME),
    'menu_name' => __( 'Attributes', AZ_THEME_NAME)
); 	

register_taxonomy('attributes',
	array('team'),
	array('hierarchical' => true,
    'labels' => $attributes_labels,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'attributes' )
));


/*-----------------------------------------------------------------------------------*/
/*	Portfolio Taxanomy / Portfolio Meta / Get Link Back
/*-----------------------------------------------------------------------------------*/

// Portfolio Meta
include("framework/meta/portfolio-meta.php");


// Get Link Back
function get_portfolio_page_link($post_id) {
    global $wpdb;
	
    $results = $wpdb->get_results("SELECT post_id FROM $wpdb->postmeta
    WHERE meta_key='_wp_page_template' AND meta_value='template-portfolio.php'");

    foreach ($results as $result) 
    {
        $page_id = $result->post_id;
    }
	
    return get_page_link($page_id);
}


// Taxanomy Register
function portfolio_register() {  
    	 
	 $portfolio_labels = array(
	 	'name' => __( 'Portfolio', 'taxonomy general name', AZ_THEME_NAME),
		'singular_name' => __( 'Portfolio Item', AZ_THEME_NAME),
		'search_items' =>  __( 'Search Portfolio Item', AZ_THEME_NAME),
		'all_items' => __( 'Portfolio', AZ_THEME_NAME),
		'parent_item' => __( 'Parent Portfolio Item', AZ_THEME_NAME),
		'edit_item' => __( 'Edit Portfolio Item', AZ_THEME_NAME),
		'update_item' => __( 'Update Portfolio Item', AZ_THEME_NAME),
		'add_new_item' => __( 'Add New Portfolio Item', AZ_THEME_NAME)
	 );
	 
	 $options = get_option('anubis'); 
     $custom_slug = null;		
	 
	 if(!empty($options['portfolio_rewrite_slug'])) $custom_slug = $options['portfolio_rewrite_slug'];
	
	 $args = array(
			'labels' => $portfolio_labels,
			'rewrite' => array('slug' => $custom_slug,'with_front' => false),
			'singular_label' => __('Project', AZ_THEME_NAME),
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'hierarchical' => false,
			'menu_position' => 8,
			'menu_icon' => AZ_FRAMEWORK_DIRECTORY . 'assets/img/icons/portfolio.png',
			'supports' => array('title', 'editor', 'thumbnail')  
       );  
  
    register_post_type( 'portfolio' , $args );  
}  
add_action('init', 'portfolio_register');

$categories_portfolio_labels = array(
	'name' => __( 'Project Categories', AZ_THEME_NAME),
	'singular_name' => __( 'Project Category', AZ_THEME_NAME),
	'search_items' =>  __( 'Search Project Categories', AZ_THEME_NAME),
	'all_items' => __( 'All Project Categories', AZ_THEME_NAME),
	'parent_item' => __( 'Parent Project Category', AZ_THEME_NAME),
	'edit_item' => __( 'Edit Project Category', AZ_THEME_NAME),
	'update_item' => __( 'Update Project Category', AZ_THEME_NAME),
	'add_new_item' => __( 'Add New Project Category', AZ_THEME_NAME),
    'menu_name' => __( 'Project Categories', AZ_THEME_NAME)
); 	

register_taxonomy("project-category", 
		array("portfolio"), 
		array("hierarchical" => true, 
				'labels' => $categories_portfolio_labels,
				'show_ui' => true,
    			'query_var' => true,
				'rewrite' => array( 'slug' => 'project-category' )
));

$attributes_portfolio_labels = array(
	'name' => __( 'Project Attributes', AZ_THEME_NAME),
	'singular_name' => __( 'Project Attribute', AZ_THEME_NAME),
	'search_items' =>  __( 'Search Project Attributes', AZ_THEME_NAME),
	'all_items' => __( 'All Project Attributes', AZ_THEME_NAME),
	'parent_item' => __( 'Parent Project Attribute', AZ_THEME_NAME),
	'edit_item' => __( 'Edit Project Attribute', AZ_THEME_NAME),
	'update_item' => __( 'Update Project Attribute', AZ_THEME_NAME),
	'add_new_item' => __( 'Add New Project Attribute', AZ_THEME_NAME),
	'new_item_name' => __( 'New Project Attribute', AZ_THEME_NAME),
    'menu_name' => __( 'Project Attributes', AZ_THEME_NAME)
); 	

register_taxonomy('project-attribute',
	array('portfolio'),
	array('hierarchical' => true,
    'labels' => $attributes_portfolio_labels,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'project-attribute' )
));


if ( !function_exists( 'az_portfolio_header' ) ) {
    function az_portfolio_header($postid) {
		
		global $options;
		global $post;
		
    	$bg = get_post_meta($postid, '_az_portfolio_header_bg', true);
		$title_page = get_post_meta($postid, '_az_portfolio_title_page', true);
		$rev_slider_alias = get_post_meta($post->ID, '_az_portfolio_intro_slider_header', true);
			
		if( !empty($bg) ) { 
    ?>	
        <section id="image-static">
            <div class="fullimage-container">
                <div class="pattern"></div>
                <img src="<?php echo $bg; ?>" alt="<?php the_title(); ?>" />
            </div>
        </section>
	
	    <?php } else if( !empty($rev_slider_alias) ) { ?>
        	<section id="slider-header">
	    		<?php echo do_shortcode('[rev_slider '.$rev_slider_alias.']'); ?>
	    	</section>
            
		<?php } elseif( !empty($title_page) ) { ?>
        
        <section id="title-page">
            <h2><?php echo $title_page; ?></h2>
            <span class="arrow"></span>
        </section>
        
        <?php } else { ?>
				
		<section id="title-page">
            <h2><?php echo the_title(); ?></h2>
            <span class="arrow"></span>
        </section>
        
		<?php }
    }
}


/*-----------------------------------------------------------------------------------*/
/*	Post Meta / Post Header
/*-----------------------------------------------------------------------------------*/

// Post Meta
include("framework/meta/post-meta.php");

if ( !function_exists( 'az_post_header' ) ) {
    function az_post_header($postid) {
		
		global $options;
		global $post;
		
    	$bg_post = get_post_meta($postid, '_az_post_header_bg', true);
		$rev_slider_alias_post = get_post_meta($post->ID, '_az_post_intro_slider_header', true);;
		$author_id = $post->post_author;
			
		if( !empty($bg_post) ) { 
    ?>	
        <section id="image-static">
            <div class="fullimage-container">
                <div class="pattern"></div>
                <img src="<?php echo $bg_post; ?>" alt="<?php the_title(); ?>" />
            </div>
        </section>
        
        <?php } else if( !empty($rev_slider_alias_post) ) { ?>
        	<section id="slider-header">
	    		<?php echo do_shortcode('[rev_slider '.$rev_slider_alias_post.']'); ?>
	    	</section>
        
		<?php } elseif ( !$bg_post ) { ?> 
        	<?php
				$no_featured = 'no-featured-post';
			?>
        <?php } ?>
			
        <section id="intro-box" class="single-post <?php echo $no_featured; ?>">
        	<span class="arrow"></span>
        	<div class="container">
            	<div class="row">
                	<div class="span12">
                    	<h2 class="entry-title"><?php the_title(); ?></h2>
                        <div class="entry-meta">
                        	<span class="author">By <?php the_author_meta( 'display_name', $author_id ); ?></span>
                            <span class="dividers">/</span>
                            <span class="published"><?php the_time( get_option('date_format') ); ?></span>
                            <span class="dividers">/</span>
                            <span class="comment-count"><?php comments_popup_link(__('No Comments', AZ_THEME_NAME), __('1 Comment', AZ_THEME_NAME), __('% Comments', AZ_THEME_NAME)); ?></span>
                            
							<?php edit_post_link( __('Edit', AZ_THEME_NAME), ' / <span class="edit-post">', '</span>' ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
            

   <?php }
}

/*-----------------------------------------------------------------------------------*/
/*	Post Audio
/*-----------------------------------------------------------------------------------*/

if ( !function_exists( 'az_audio' ) ) {
    function az_audio($postid) {
	
    	$mp3 = get_post_meta($postid, '_az_audio_mp3', true);
    	$ogg = get_post_meta($postid, '_az_audio_ogg', true);
		$poster_audio = get_post_meta($postid, '_az_audio_poster_url', true);
    	
    ?>
		
    		<script type="text/javascript">
		
    			jQuery(document).ready(function($){
	
    				if( $().jPlayer ) {
    					$("#jquery_jplayer_<?php echo $postid; ?>").jPlayer({
    						ready: function () {
    							$(this).jPlayer("setMedia", {
    							    <?php if($mp3 != '') : ?>
    								mp3: "<?php echo $mp3; ?>",
    								<?php endif; ?>
    								<?php if($ogg != '') : ?>
    								oga: "<?php echo $ogg; ?>",
    								<?php endif; ?>
    								<?php if ($poster_audio != '') : ?>
    								poster: "<?php echo $poster_audio; ?>",
    								<?php endif; ?>
									end: ""
    							});
    						},
    						size: {
							  width: "100%",
							  height: "auto"
							},
    						swfPath: "<?php echo get_template_directory_uri(); ?>/_include/js",
    						cssSelectorAncestor: "#jp_interface_<?php echo $postid; ?>",
    						supplied: "<?php if($ogg != '') : ?>oga,<?php endif; ?><?php if($mp3 != '') : ?>mp3<?php endif; ?>"
    					});
					
    				}
    			});
    		</script>
		
    	    <div id="jquery_jplayer_<?php echo $postid; ?>" class="jp-jplayer jp-jplayer-audio"></div>

            <div class="jp-audio-container">
                <div class="jp-audio">
                    <div id="jp_interface_<?php echo $postid; ?>" class="jp-interface">
                        <ul class="jp-controls">
                            <li><a href="#" class="jp-play" tabindex="1">play</a></li>
                            <li><a href="#" class="jp-pause" tabindex="1">pause</a></li>
                            <li><a href="#" class="jp-mute" tabindex="1">mute</a></li>
                            <li><a href="#" class="jp-unmute" tabindex="1">unmute</a></li>
                        </ul>
                        <div class="jp-progress">
                            <div class="jp-seek-bar">
                                <div class="jp-play-bar"></div>
                            </div>
                        </div>
                        <div class="jp-volume-bar-container">
                            <div class="jp-volume-bar">
                                <div class="jp-volume-bar-value"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    	<?php 
    }
}


/*-----------------------------------------------------------------------------------*/
/*	Post Video
/*-----------------------------------------------------------------------------------*/

if ( !function_exists( 'az_video' ) ) {
    function az_video($postid) { 
	
    	$m4v = get_post_meta($postid, '_az_video_m4v', true);
    	$ogv = get_post_meta($postid, '_az_video_ogv', true);
    	$poster_video = get_post_meta($postid, '_az_video_poster_url', true);

    ?>
    <script type="text/javascript">
    	jQuery(document).ready(function($){
		
    		if( $().jPlayer ) {
    			$("#jquery_jplayer_<?php echo $postid; ?>").jPlayer({
    				ready: function () {
    					$(this).jPlayer("setMedia", {
    						<?php if($m4v != '') : ?>
    						m4v: "<?php echo $m4v; ?>",
    						<?php endif; ?>
    						<?php if($ogv != '') : ?>
    						ogv: "<?php echo $ogv; ?>",
    						<?php endif; ?>
    						<?php if ($poster_video != '') : ?>
    						poster: "<?php echo $poster_video; ?>"
    						<?php endif; ?>
    					});
    				},
    				size: {
					  cssClass: "jp-jplayer-video",
			          width: "100%",
			          height: "auto"
			        },
    				swfPath: "<?php echo get_template_directory_uri(); ?>/_include/js",
    				cssSelectorAncestor: "#jp_interface_<?php echo $postid; ?>",
    				supplied: "<?php if($m4v != '') : ?>m4v, <?php endif; ?><?php if($ogv != '') : ?>ogv<?php endif; ?>"
    			});
    		}
    	});
    </script>

    <div id="jquery_jplayer_<?php echo $postid; ?>" class="jp-jplayer jp-jplayer-video"></div>

    <div class="jp-video-container">
        <div class="jp-video">
            <div id="jp_interface_<?php echo $postid; ?>" class="jp-interface">
                <ul class="jp-controls">
                	<li><div class="seperator-first"></div></li>
                    <li><div class="seperator-second"></div></li>
                    <li><a href="#" class="jp-play" tabindex="1">play</a></li>
                    <li><a href="#" class="jp-pause" tabindex="1">pause</a></li>
                    <li><a href="#" class="jp-mute" tabindex="1">mute</a></li>
                    <li><a href="#" class="jp-unmute" tabindex="1">unmute</a></li>
                </ul>
                <div class="jp-progress">
                    <div class="jp-seek-bar">
                        <div class="jp-play-bar"></div>
                    </div>
                </div>
                <div class="jp-volume-bar-container">
                    <div class="jp-volume-bar">
                        <div class="jp-volume-bar-value"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <?php }
}

/*-----------------------------------------------------------------------------------*/
/*  Custom Output Page Title
/*-----------------------------------------------------------------------------------*/

if( !function_exists( 'az_custom_get_page_title' ) ) {
    function az_custom_get_page_title() {
        $page_title = '';
        if( is_archive() ) {
                if( is_category() ) {
                    $page_title = sprintf( __( 'All posts in %s', AZ_THEME_NAME ), single_cat_title('', false) );
                } elseif( is_tag() ) {
                    $page_title = sprintf( __( 'All posts in %s', AZ_THEME_NAME ), single_tag_title('', false) );
                } elseif( is_date() ) {
                    if( is_month() ) {
                        $page_title = sprintf( __( 'Archive for %s', AZ_THEME_NAME ), get_the_time( 'F, Y' ) );
                    } elseif( is_year() ) {
                        $page_title = sprintf( __( 'Archive for %s', AZ_THEME_NAME ), get_the_time( 'Y' ) );
                    } elseif( is_day() ) {
                        $page_title = sprintf( __('Archive for %s', AZ_THEME_NAME ), get_the_time( get_option('date_format') ) );
                    } else {
                        $page_title = __('Blog Archives', AZ_THEME_NAME);
                    }
                } elseif( is_author() ) {
                    if(get_query_var('author_name')) {
                        $curauth = get_user_by( 'login', get_query_var('author_name') );
                    } else {
                        $curauth = get_userdata(get_query_var('author'));
                    }
                    $page_title = $curauth->display_name;
                } 
            } 
		elseif( is_search() ) {
       		$page_title = sprintf( __( 'Search Results for &#8220;%s&#8221;', AZ_THEME_NAME ), get_search_query() );
        } 

        return $page_title;
    }
}

/*-----------------------------------------------------------------------------------*/
/*	Navigation
/*-----------------------------------------------------------------------------------*/

if ( !function_exists( 'az_pagination' ) ) {
	
	function az_pagination() {	
		global $options;
		
		if( get_next_posts_link() || get_previous_posts_link() ) { 
			echo '<div class="pagination-blog">
				  <ul class="pagination">
				  <li class="prev">'.get_previous_posts_link('<i class="font-icon-arrow-simple-left"></i> Older Entries').'</li>
				  <li class="next">'.get_next_posts_link('<i class="font-icon-arrow-simple-right"></i> New Entries').'</li>
				  </ul>
				  </div>';
		
		}
	}
}

/*-----------------------------------------------------------------------------------*/
/*	Comment Styling
/*-----------------------------------------------------------------------------------*/

if ( !function_exists( 'az_comment' ) ) {
	function az_comment($comment, $args, $depth) {
	
        $isByAuthor = false;

        if($comment->comment_author_email == get_the_author_meta('email')) {
            $isByAuthor = true;
        }

        $GLOBALS['comment'] = $comment; ?>
        <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
            <div id="comment-<?php comment_ID(); ?>">              
                <div class="comment-side">
                	<?php echo get_avatar($comment,$size='50'); ?>
                </div>
             
                <div class="comment-cont">
                    <div class="comment-author">
                        <cite class="fn"><?php echo get_comment_author_link(); ?></cite><?php if( $isByAuthor ) { ?><span class="badge"> <?php _e('Author', AZ_THEME_NAME) ?></span><?php } ?>
                    </div>
                    
                    <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf( __('%1$s at %2$s', 'zilla'), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link(__('Edit', 'zilla'), ' / ','') ?> / <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?></div>
                    
                    <?php if ($comment->comment_approved == '0') : ?>
                        <em class="moderation"><?php  _e('Your comment is awaiting moderation.', AZ_THEME_NAME) ?></em><br />
                    <?php endif; ?>
                    
                    <div class="comment-body">
                        <?php comment_text() ?>
                    </div>
                </div>
            </div>
	<?php
	}
}

/*-----------------------------------------------------------------------------------*/
/*	Seperated Pings Styling
/*-----------------------------------------------------------------------------------*/

if ( !function_exists( 'az_comment_list_pings' ) ) {
	function az_comment_list_pings($comment, $args, $depth) {
	    $GLOBALS['comment'] = $comment; ?>
		<li id="comment-<?php comment_ID(); ?>"><?php comment_author_link(); ?>
		<?php 
	}
}



/*-----------------------------------------------------------------------------------*/
/*	Social Profiles
/*-----------------------------------------------------------------------------------*/

$socials_profiles = array ('500px', 'addthis', 'behance', 'bebo', 'blogger', 'deviant-art', 'digg', 'dribbble', 'email', 'envato', 'evernote', 'facebook', 'flickr', 'forrst', 'github', 'google-plus', 'grooveshark', 'instagram', 'last-fm', 'linkedin', 'myspace', 'paypal', 'photobucket', 'pinterest', 'quora', 'share-this', 'skype', 'soundcloud', 'stumbleupon', 'tumblr', 'twitter', 'viddler', 'vimeo', 'virb', 'wordpress', 'yahoo', 'yelp', 'youtube', 'zerply');



/*-----------------------------------------------------------------------------------*/
/*	Include the framework
/*-----------------------------------------------------------------------------------*/

$tempdir = get_template_directory();
require_once($tempdir .'/framework/options.php');
require_once($tempdir .'/framework/plugin-activation/init.php');

?>