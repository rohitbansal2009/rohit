<?php
session_start();
include "../config.php";
include_once "../ezSQL/shared/ez_sql_core.php";
include_once "../ezSQL/mysql/ez_sql_mysql.php";
include_once "../mail/class.phpmailer.php";

if(isset($_POST['ssn']) && isset($_POST['name']) && isset($_POST['address']) && isset($_POST['city']) && isset($_POST['postno']) && isset($_POST['email']) && isset($_POST['phone'])){
	$ssn = $_POST['ssn'];
	$name = $_POST['name'];
	$address = $_POST['address'];
	$city = $_POST['city'];
	$postno = $_POST['postno'];
	$emailad = $_POST['email'];
	$phoneno = $_POST['phone'];
	if(!empty($_POST['nanosim']))
	{
		$nanosim = 1;
	}
	else{
		$nanosim = 0;
	}
	
	
	$config = new config();
	$db = new ezSQL_mysql($config->db_username,$config->db_password,$config->db_database,$config->db_host);
	$ip = $_SERVER['REMOTE_ADDR'];
	
	if(!empty($_SESSION['pkg'])){
		$pkg = $_SESSION['pkg'];
		$package = $db->get_row("select * from package where id =" . $pkg);
	}
	else{
		$pkg = 0;
	}
	if(!empty($_SESSION['phn']))
	{
		$phn = $_SESSION['phn'];
		$paytype = $_SESSION['paytype'];
		$month = $_SESSION['month'];
		$phonecolor = $_SESSION['color'];
		$phoneconfig = $_SESSION['config'];
		
		$phone = $db->get_row("select * from phone where id =" . $phn);
		if(!empty($phone))
		{
			if(!empty($phoneconfig))
			{
				$config = $db->get_row("select * from phoneconfig where id = " . $phoneconfig);
				$phoneprice = $config->price_with_vat;
				$configname = $config->config;
			}
			else{
				$phoneprice = $phone->price_with_vat;
				$configname = '';
			}
			
			if(!empty($phonecolor))
			{
				$color = $db->get_row("select * from phonecolor where id = " . $phonecolor);
				$image = $color->image;
			}
			else{
				$image = $phone->image;
			}
			
			$max_partial_payment = round((($phoneprice / 24)+5/2)/5)*5;
			$max_partial_payment1 = $max_partial_payment - (50 * ($paytype - 1));
			$paynow = $phoneprice - ($max_partial_payment1 * $month);
		}
		else{
			$phn = 0;
			$paytype = 0;
			$month = 0;
			$max_partial_payment1 = 0;
			$paynow = 0;
		}
	}
	else{
		$phn = 0;
		$paytype = 0;
		$month = 0;
		$max_partial_payment1 = 0;
		$paynow = 0;
		$phonecolor = 0;
		$phoneconfig = 0;
	}
	$old_order = $db->query("select * from orders where ssn=" . $ssn);
	if(empty($old_order))
	{
		$db->query("insert into orders(ssn,name,address,city,postno,email,phoneno,ip,date,package,phone,months,paynow,monthly,color,config,nanosim) values('$ssn','$name','$address','$city','$postno','$emailad','$phoneno','$ip',now(),$pkg,$phn,$month,$paynow,$max_partial_payment1,$phonecolor,$phoneconfig,$nanosim)");
		
		$orderid = $db->insert_id;
		$headers = "MIME-Version: 1.0\r\n Content-Type: text/html; charset=UTF-8\n";
		$email = '<html><head> 
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" id="admin-bar-css"  href="http://localhost/teligooab-landingpages-53debdffae9f/wp-includes/css/admin-bar.min.css?ver=3.6" type="text/css" media="all" />
<link rel="stylesheet" id="contact-form-7-css"  href="http://localhost/teligooab-landingpages-53debdffae9f/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=3.5.1" type="text/css" media="all" />
<link rel="stylesheet" id="rs-settings-css"  href="http://localhost/teligooab-landingpages-53debdffae9f/wp-content/plugins/revslider/rs-plugin/css/settings.css?ver=3.6" type="text/css" media="all" />
<link rel="stylesheet" id="rs-captions-css"  href="http://localhost/teligooab-landingpages-53debdffae9f/wp-content/plugins/revslider/rs-plugin/css/captions.css?ver=3.6" type="text/css" media="all" />
<link rel="stylesheet" id="bootstrap-css"  href="http://localhost/teligooab-landingpages-53debdffae9f/wp-content/themes/anubis/_include/css/bootstrap.min.css?ver=3.6" type="text/css" media="all" />
<link rel="stylesheet" id="main-styles-css"  href="http://localhost/teligooab-landingpages-53debdffae9f/wp-content/themes/anubis/style.css?ver=3.6" type="text/css" media="all" />
<link rel="stylesheet" id="fancybox-css-css"  href="http://localhost/teligooab-landingpages-53debdffae9f/wp-content/themes/anubis/_include/css/fancybox/jquery.fancybox.css?ver=3.6" type="text/css" media="all" />
<link rel="stylesheet" id="font-icon-css"  href="http://localhost/teligooab-landingpages-53debdffae9f/wp-content/themes/anubis/_include/css/fonts.css?ver=3.6" type="text/css" media="all" />
<link rel="stylesheet" id="shortcode-css-css"  href="http://localhost/teligooab-landingpages-53debdffae9f/wp-content/themes/anubis/_include/css/shortcodes.css?ver=3.6" type="text/css" media="all" />
<link rel="stylesheet" id="responsive-css-css"  href="http://localhost/teligooab-landingpages-53debdffae9f/wp-content/themes/anubis/_include/css/responsive.css?ver=3.6" type="text/css" media="all" />
<link rel="stylesheet" id="validationEngine-css"  href="http://localhost/teligooab-landingpages-53debdffae9f/wp-content/themes/anubis/_include/css/validationEngine.jquery.css?ver=3.6" type="text/css" media="all" />
<link rel="stylesheet" id="dynamic_colors-css"  href="http://localhost/teligooab-landingpages-53debdffae9f/wp-content/themes/anubis/_include/css/color.css.php?ver=3.6" type="text/css" media="all" />
<link rel="stylesheet" id="custom_css-css"  href="http://localhost/teligooab-landingpages-53debdffae9f/wp-content/themes/anubis/_include/css/custom.css.php?ver=3.6" type="text/css" media="all" />
		</head><body class="page page-id-600 page-template-default logged-in admin-bar customize-support">
		<div id="content">
		<div class="container">
		
		<p style="text-align: left;">Hej isuru,</p>
<p style="text-align: left;">Vad roligt att du har valt teligoo som din mobiloperatör. Vi har nu tagit emot din beställning.<BR/>
Du finner dina orderuppgifter nedan.</p>
<p style="text-align: left;">Orderdatum: 2013-08-16<BR/>
Ordernummer: 1016<BR/>
Leveransadress:<BR/>
' . $name . '<br>
' . $address .'<br>
' . $city .'<br>
'.  $postno .'</p>
<p style="text-align: left;">Telefonnummer: ' . $phoneno . '<BR/>
E-mail: ' . $emailad . '</p>

<h2 style="text-align: left;">Din beställning:</h2>
<p style="text-align: left;"></p>
<div class="divider"></div>
<h2 style="text-align: left;">' . utf8_encode($package->name) . ' ' . $package->price_with_vat . ' kr / månad</h2>
<p style="text-align: left;">Spesifikation:</p>
<p style="text-align: left;">' . utf8_encode($package->specification) . '</p>';
		if(!empty($phone))
		{
			$email .= '<p style="text-align: left;"></p>
<div class="divider"></div>

<h2 style="text-align: left;">' . utf8_encode($phone->name . ' ' . $configname . ' ');
			if(!empty($month)){
				$email .= $max_partial_payment1;
			}
			else{
				$email .=$phoneprice;
			}
				$email .=  ' kr /';
				if(!empty($month))
			{
				$email .=  'månad  i '. $month .' månader</h2>';
			}
		}
		$email .= '<p style="text-align: left;"></p>
<div class="divider"></div>

<h3 style="text-align: left;">Totalt '. ($package->price_with_vat + $max_partial_payment1) .' kr / månad<BR/>';
if (($phoneprice - ($max_partial_payment1 * $month)) > 0){
			$email .= 'Betalas nu på mobilen: ' . ($phoneprice - ($max_partial_payment1 * $month)) . ' kr<BR/>';
}
		$email .= 'Engångsavgift på separat faktura: 0 kr för inträdesavgift ord. pris ' . $package->price_with_vat . ' kr<BR/></h3>';
		if(!empty($phone))
		{
			$email .= 'Total kostnad för mobilen: ' . $phoneprice . ' kr';
		}
		$email .= '<p style="text-align: left;"></p>
<div class="divider"></div>
<p style="text-align: left;">Tack för att ni har valt teligoo!</p>
<p style="text-align: left;">Kontakta oss:<BR/>
Telefon: 0770 - 32 22 22<BR/>
e-post: kundtjanst@teligoo.se<BR/>
Hemsida: teligoo.se</p>
<p style="text-align: left;"><a href="http://localhost/teligooab-landingpages-53debdffae9f/wp-content/uploads/2013/08/logo_cmyk.png"><img class="alignnone size-medium wp-image-704" alt="logo_cmyk" src="http://localhost/teligooab-landingpages-53debdffae9f/wp-content/uploads/2013/08/logo_cmyk-300x77.png" width="300" height="77" /></a></p></div></div></body></html>';

//send thankyou mail
		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->SMTPAuth   = true; 
		$mail->SMTPSecure = "tls"; 
		$mail->Host       = "email-smtp.us-east-1.amazonaws.com";
		$mail->Username   = "AKIAIX36SD5ZQQTGLGMA";
		$mail->Password   = "AmstjLj2uDgdMEf2i6B5t0Q3ZqfpEa1BSg1/oCykdkWh";
		//$mail->mailHeader = $headers;
		$mail->CharSet = "UTF-8";
		
		$mail->SetFrom('info@teligoo.se', 'Teligoo');
		$mail->Subject = "Teligoo Order confirmation";
		

		//$body = eregi_replace("[]",'',$email);
		$mail->MsgHTML($email);
		
		$mail->AddAddress($emailad, $name); 
		$mail->AddAddress("john@teligoo.se", "sales"); 
		if ($mail->Send()) { 
			//echo "Message sent!"; 
		}
		else{
			echo "order Mailer Error please contact john@teligoo.se"; 
		} 


		//echo "insert into orders(ssn,name,address,city,postno,email,phoneno,ip,date,package,phone,months,paynow,monthly) values('$ssn','$name','$address','$city','$postno','$email','$phoneno','$ip',now(),$pkg,$phn,$month,$paynow,$max_partial_payment1)";
		header( 'Location: /thank-you/?odr=' . $orderid) ;
	}
	else{
		//alredy has order
		header( 'Location: /order-error/' ) ;
	}
}
else{
	header( 'Location: /order-page/' ) ;
}