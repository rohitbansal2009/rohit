<?php 

$options = get_option('anubis'); 

if( !empty($options['enable-twitter-footer-area']) && $options['enable-twitter-footer-area'] == 1) { ?>
	
<!-- Start Twitter Feed -->
<section id="twitter-feed">
<span class="arrow-down"></span>
	<div class="container">
    	<div class="row">
        	<div class="span12">
            	<ul class="tweet_list">
                	<?php
						$num_tweet = 1;
						$twitter_username = $options['twitter-footer-username'];
						
						$tweets = getTweets($num_tweet, $twitter_username);
						foreach($tweets as $tweet) {
						
						echo '
							<li>
								<span class="tweet_text">' . TwitterFilter($tweet['text']) . '</span>
								<span class="tweet_time"><a href="https://twitter.com/bluxart/status/' . $tweet['id_str'] . '">' . date('j F o \a\t g:i A', strtotime($tweet['created_at'])) . '</a></span>
							</li>';
						
						}
					?>
                </ul>
            </div>
        </div>
    </div>	
</section>
<!-- End Twitter Feed -->
<?php } ?>

<?php if( !empty($options['enable-main-footer-area']) && $options['enable-main-footer-area'] == 1) { ?>
<!-- Start Footer Area -->

	<?php if( !empty($options['enable-footer-social-area']) && $options['enable-footer-social-area'] == 1) { ?>
    <!-- Start Footer with Social Icons -->
    <footer>
    	
    	<?php if( $options['enable-twitter-footer-area'] == 0) { ?>
        <span class="arrow-down"></span>
        <?php } ?>
        
        <div class="container">
            <div class="row">
                <nav id="social-footer" class="span12">
                    <ul>
                        <?php
							global $socials_profiles;
							
                            foreach($socials_profiles as $social_profile):
                                if( $options[$social_profile.'-url'] )
                                {
                                    echo '<li><a href="'.$options[$social_profile.'-url'].'" target="_blank"><i class="font-icon-social-'.$social_profile.'"></i></a></li>';
                                }
                            endforeach;
                        ?>
                    </ul>
                </nav>
            </div>
        </div>
    </footer>
    <!-- End Footer with Social Icons -->
    <?php } ?>
    
    <?php if( !empty($options['enable-footer-widget-area']) && $options['enable-footer-widget-area'] == 1) { ?>
    <!-- Start Footer with Widgets -->
    <footer>
    	<div class="footer-widgets">
    
    	<?php if( $options['enable-twitter-footer-area'] == 0) { ?>
        <span class="arrow-down"></span>
        <?php } ?>
        
        <div class="container">
        	<div class="row">
                <div class="span4">
                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                    <?php dynamic_sidebar('footer-area-one'); ?>
                <?php } ?>
                </div>
                <div class="span4">
                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                    <?php dynamic_sidebar('footer-area-two'); ?>
                <?php } ?>
                </div>
                <div class="span4">
                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                    <?php dynamic_sidebar('footer-area-three'); ?>
                <?php } ?>
                </div>
        	</div>
             
        </div>
        
        </div>
    </footer>
    <!-- End Footer with Widgets -->
  
    <?php } ?>
             
  
<!-- End Footer Area -->
<?php } ?>
            
  

<!-- Start Footer Credits -->
<section id="footer-credits">
	<div class="container">
		<div class="row">
			<div class="span12">
            	<p class="credits"><?php wp_nav_menu(array('menu' => 'foote_menu')); ?></p>
				<p class="credits">&copy; <?php echo date('Y') . ' ' . get_bloginfo('name'); ?>. <?php if(!empty($options['footer-copyright-text'])) echo $options['footer-copyright-text']; ?></p>
			</div>
		</div>
	</div>
</section>

<!-- Start Footer Credits -->

<?php if( !empty($options['enable-back-to-top']) && $options['enable-back-to-top'] == 1) { ?>
<!-- Back To Top -->
<a id="back-to-top" href="#">
	<i class="font-icon-arrow-simple-up"></i>
</a>
<!-- End Back to Top -->
<?php } ?>

<?php if(!empty($options['tracking-code'])) echo $options['tracking-code']; ?> 

<?php wp_footer(); ?>	
<a href="#" class="right_img"><img src="<?php bloginfo(template_directory); ?>/images/for.jpg" alt=""></a> <a href="#" class="right_img private"><img src="<?php bloginfo(template_directory); ?>/images/pravite.jpg" alt=""></a>
</body>
</html>