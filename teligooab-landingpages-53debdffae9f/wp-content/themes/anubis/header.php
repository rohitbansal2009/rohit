<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

<!-- Meta Tags -->
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<!-- Mobile Specifics -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="HandheldFriendly" content="true"/>
<meta name="MobileOptimized" content="320"/>   

<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,900' rel='stylesheet' type='text/css'>


<!-- Mobile Internet Explorer ClearType Technology -->
<!--[if IEMobile]>  <meta http-equiv="cleartype" content="on">  <![endif]-->

<?php $options = get_option('anubis'); 

if( !empty($options['use-logo'])) {
	$logo = $options['logo'];
	$retina_logo = $options['retina-logo'];
	

	if ($retina_logo == "") {
		$retina_logo = $logo;
		
	}
}

?>

<?php if(!empty($options['favicon'])) { ?>
<!--Shortcut icon-->
<link rel="shortcut icon" href="<?php echo $options['favicon']?>" />
<?php } ?>

<!-- Title -->
<title><?php wp_title('|',true,'right'); ?><?php bloginfo('name'); ?></title>

<!-- RSS & Pingbacks -->
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?> RSS Feed" href="<?php bloginfo( 'rss2_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<!-- Google Font -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<!-- Header -->
<header>
	<div class="container">
    	<div class="row">
        
        	<div class="span3">
            	<div id="logo">
        			<a href="<?php echo home_url(); ?>">
                    	<?php
							if( !empty($options['use-logo'])) { ?>
								<img class="standard" src="<?php echo $logo; ?>" alt="<?php bloginfo('name'); ?>" />
                                <img class="retina" src="<?php echo $retina_logo; ?>" alt="<?php bloginfo('name'); ?>" />
						<?php } else { ?>
                            	<?php bloginfo('name'); ?>                            
                    	<?php } ?>
					</a>
        		</div>
            </div>
            
            <div class="span9">
            	<!-- Mobile Menu -->
            	<a id="mobile-nav" class="menu-nav" href="#menu-nav"><span class="menu-icon"></span></a>
                
                <!-- Standard Menu -->
        		<nav id="menu">
                    <ul id="menu-nav">
                        <?php 
						if(has_nav_menu('primary_menu')) {
							wp_nav_menu( array('theme_location' => 'primary_menu', 'menu' => 'Primary Menu', 'container' => '', 'items_wrap' => '%3$s' ) ); 
						}
						else {
							echo '<li><a href="#">No menu assigned!</a></li>';
						}
						?>	
                    </ul>
                </nav>
        	</div>
            
        </div>
    </div>
</header>
<!-- End Header -->