<?php get_header(); ?>

<?php az_page_header($post->ID); ?>

<div id="content">
	<div class="container">
    	<section id="single-team">
        	<div class="row">
            
            	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
                
                <?php
					$attrs = get_the_terms( $post->ID, 'attributes' );
					$attributes_fields = NULL;
					
					if ( !empty($attrs) ){
					 foreach ( $attrs as $attr ) {
					   $attributes_fields[] = $attr->name;
					 }
					 
					 $on_attributes = join( " - ", $attributes_fields );
					}
				?>
                
                <div class="span6">
                    <div class="team-img">
                        <?php echo get_the_post_thumbnail($post->ID, 'full', array('title' => '')); ?>
                    </div>
            	</div>
                
                <div class="span6">
                	<div class="team-name">
                    	<h2><?php the_title(); ?></h2>
						<h3><?php echo $on_attributes; ?></h3>
                    </div>
                    
                    <div class="team-description">
						<?php edit_post_link( __('Edit', AZ_THEME_NAME), '<span class="edit-post">[', ']</span>' ); ?>
                    	<?php the_content(); ?>
                    </div>
                    
                    <div class="team-social">
                    	<ul>
						<?php
                            foreach($socials_profiles as $social_profile):
                                if(get_post_meta($post->ID, '_az_profile_'.$social_profile, true))
                                {
                                    echo '<li><a href="'.get_post_meta($post->ID, '_az_profile_'.$social_profile, true).'" target="_blank"><i class="font-icon-social-'.$social_profile.'"></i></a></li>';
                                }
                            endforeach;
                        ?>
                        </ul>
                    </div>
                
                </div>
                <?php endwhile; endif; ?>
                
            </div>
            
            <?php $team_link = get_team_page_link(get_the_ID()); ?>
            
            <div class="row">
            	<div class="span12">
                	<div class="paginate-team">
                    	<div class="separator"></div>
                    	<ul>
                            <li class="prev"><?php previous_post_link('%link', '<i class="font-icon-arrow-simple-left"></i>%title'); ?></li>
                            <li class="back-page"><a href="<?php echo $team_link; ?>"><i class="font-icon-group"></i><?php _e('Back to Team', AZ_THEME_NAME); ?></a></li> 
                            <li class="next"><?php next_post_link('%link', '<i class="font-icon-arrow-simple-right"></i>%title'); ?></li> 
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
	
<?php get_footer(); ?>