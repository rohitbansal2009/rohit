<?php get_header(); ?>

<?php
$options = get_option('anubis'); 

// Image Static and Intro Box
if( !empty($options['enable-image-static-blog-area']) && $options['enable-image-static-blog-area'] == 1) { ?>

	<?php if(!empty($options['image-static'])) { ?>
        <section id="image-static">
            <div class="fullimage-container">
                <div class="pattern"></div>
                <img src="<?php echo $options['image-static']?>" alt="<?php echo get_bloginfo('name'); ?>" />
            </div>
        </section>
        
            <?php if(!empty($options['image-intro-box-text'])) { ?>
            <section id="intro-box">
                <div class="container">
                    <div class="row">
                        <div class="span12">
                            <h3><?php echo $options['image-intro-box-text']?></h3>
                            <span class="arrow"></span>
                        </div>
                    </div>
                </div>
            </section>
            <?php } ?>
        
    <?php } ?>

<?php } 

// Slider and Intro Box

if( !empty($options['enable-slider-blog-area']) && $options['enable-slider-blog-area'] == 1) { ?>
	
    <?php if(!empty($options['slider-alias'])) { ?>
        	<section id="slider-header">
	    		<?php echo do_shortcode('[rev_slider '.$options['slider-alias'].']'); ?>
	    	</section>
        
            <?php if(!empty($options['slider-intro-box-text'])) { ?>
            <section id="intro-box">
                <div class="container">
                    <div class="row">
                        <div class="span12">
                            <h3><?php echo $options['slider-intro-box-text']?></h3>
                            <span class="arrow"></span>
                        </div>
                    </div>
                </div>
            </section>
            <?php } ?>
        
    <?php } ?>

<?php } else { ?>

<section id="title-page">
    <h2><?php echo $options['blog-text']?></h2>
    <span class="arrow"></span>
</section>
	
<?php } ?>

<div id="content">
	<section id="blog" class="blog-post-full">
        <div class="container">
            <div class="row">
            	<div class="span12">
            
					<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
                    
                    	<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">	
                    
                        	<?php 
							$format = get_post_format(); 
							get_template_part( 'content', $format );
							?>
                        
                        	<div class="separator"></div>
                        </article>
                    
                    <?php endwhile; endif; ?>
                    
                    <?php az_pagination(); ?>
                   
                  </div>  
        	</div> 
        </div>
	</section>
</div>
    
<?php get_footer(); ?>
