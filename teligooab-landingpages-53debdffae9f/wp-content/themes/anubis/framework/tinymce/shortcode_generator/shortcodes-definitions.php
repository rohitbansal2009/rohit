<?php

#-----------------------------------------------------------------
# Columns
#-----------------------------------------------------------------

//Half
$az_shortcodes['header_1'] = array( 
	'type'=>'heading', 
	'title'=>__('Columns', AZ_THEME_NAME)
);

// Row
$az_shortcodes['row'] = array(  
	'type'=>'checkbox', 
	'title'=>__('Row', AZ_THEME_NAME ),
	'attr'=>array( 
		'class'=>array('type'=>'text', 'title'=>__('Class', AZ_THEME_NAME)) 
	)
);

// 1/12 Column
$az_shortcodes['span1'] = array(  
	'type'=>'checkbox', 
	'title'=>__('1 Column', AZ_THEME_NAME ),
	'attr'=>array( 
		'class'=>array('type'=>'text', 'title'=>__('Class', AZ_THEME_NAME)) 
	)
);

// 2/12 Column
$az_shortcodes['span2'] = array(  
	'type'=>'checkbox', 
	'title'=>__('2 Column', AZ_THEME_NAME ),
	'attr'=>array( 
		'class'=>array('type'=>'text', 'title'=>__('Class', AZ_THEME_NAME)) 
	)
);

// 3/12 Column
$az_shortcodes['span3'] = array(  
	'type'=>'checkbox', 
	'title'=>__('3 Column', AZ_THEME_NAME ),
	'attr'=>array( 
		'class'=>array('type'=>'text', 'title'=>__('Class', AZ_THEME_NAME)) 
	)
);

// 4/12 Column
$az_shortcodes['span4'] = array(  
	'type'=>'checkbox', 
	'title'=>__('4 Column', AZ_THEME_NAME ),
	'attr'=>array( 
		'class'=>array('type'=>'text', 'title'=>__('Class', AZ_THEME_NAME)) 
	)
);

// 5/12 Column
$az_shortcodes['span5'] = array(  
	'type'=>'checkbox', 
	'title'=>__('5 Column', AZ_THEME_NAME ),
	'attr'=>array( 
		'class'=>array('type'=>'text', 'title'=>__('Class', AZ_THEME_NAME)) 
	)
);

// 6/12 Column
$az_shortcodes['span6'] = array(  
	'type'=>'checkbox', 
	'title'=>__('6 Column', AZ_THEME_NAME ),
	'attr'=>array( 
		'class'=>array('type'=>'text', 'title'=>__('Class', AZ_THEME_NAME)) 
	)
);

// 7/12 Column
$az_shortcodes['span7'] = array(  
	'type'=>'checkbox', 
	'title'=>__('7 Column', AZ_THEME_NAME ),
	'attr'=>array( 
		'class'=>array('type'=>'text', 'title'=>__('Class', AZ_THEME_NAME)) 
	)
);

// 8/12 Column
$az_shortcodes['span8'] = array(  
	'type'=>'checkbox', 
	'title'=>__('8 Column', AZ_THEME_NAME ),
	'attr'=>array( 
		'class'=>array('type'=>'text', 'title'=>__('Class', AZ_THEME_NAME)) 
	)
);

// 9/12 Column
$az_shortcodes['span9'] = array(  
	'type'=>'checkbox', 
	'title'=>__('9 Column', AZ_THEME_NAME ),
	'attr'=>array( 
		'class'=>array('type'=>'text', 'title'=>__('Class', AZ_THEME_NAME)) 
	)
);

// 10/12 Column
$az_shortcodes['span10'] = array(  
	'type'=>'checkbox', 
	'title'=>__('10 Column', AZ_THEME_NAME ),
	'attr'=>array( 
		'class'=>array('type'=>'text', 'title'=>__('Class', AZ_THEME_NAME)) 
	)
);

// 11/12 Column
$az_shortcodes['span11'] = array(  
	'type'=>'checkbox', 
	'title'=>__('11 Column', AZ_THEME_NAME ),
	'attr'=>array( 
		'class'=>array('type'=>'text', 'title'=>__('Class', AZ_THEME_NAME)) 
	)
);

// 12/12 Column
$az_shortcodes['span12'] = array(  
	'type'=>'checkbox', 
	'title'=>__('12 Column', AZ_THEME_NAME ),
	'attr'=>array( 
		'class'=>array('type'=>'text', 'title'=>__('Class', AZ_THEME_NAME)) 
	)
);

#-----------------------------------------------------------------
# Elements 
#-----------------------------------------------------------------

$az_shortcodes['header_6'] = array( 
	'type'=>'heading', 
	'title'=>__('Elements', AZ_THEME_NAME )
);

// Page Titlte
$az_shortcodes['page_title'] = array( 
	'type'=>'simple', 
	'title'=>__('Page Title Heading', AZ_THEME_NAME ), 
	'attr'=>array( 
		'class'=>array('type'=>'text', 'title'=>__('Class', AZ_THEME_NAME)) 
	)
);

// Divider
$az_shortcodes['divider'] = array( 
	'type'=>'regular', 
	'title'=>__('Divider', AZ_THEME_NAME ), 
	'attr'=>array( 
		'class'=>array('type'=>'text', 'title'=>__('Class', AZ_THEME_NAME)) 
	)
);

// Accordion
$az_shortcodes['accordion_section'] = array( 
	'type'=>'dynamic', 
	'title'=>__('Accordion Section', AZ_THEME_NAME ), 
	'attr'=>array(
		'accordions'=>array('type'=>'custom')
	)
);

// Toggle
$az_shortcodes['toggle_section'] = array( 
	'type'=>'dynamic', 
	'title'=>__('Toggle Section', AZ_THEME_NAME ), 
	'attr'=>array(
		'toggles'=>array('type'=>'custom')
	)
);

// Tabs
$az_shortcodes['tab_section'] = array( 
	'type'=>'dynamic', 
	'title'=>__('Tab Section', AZ_THEME_NAME ), 
	'attr'=>array(
		'tabs'=>array('type'=>'custom')
	)
);

// Alert Box
$az_shortcodes['alert_box'] = array( 
	'type'=>'checkbox', 
	'title'=>__('Alert Box', AZ_THEME_NAME), 
	'attr'=>array(
		'class'=>array('type'=>'text', 'title'=>__('Class', AZ_THEME_NAME)), 
		'mode'=>array(
			'type'=>'radio', 
			'title'=>__('Type', AZ_THEME_NAME), 
			'opt'=>array(
				'standard'=>'Standard',
				'warning'=>'Warning',
				'error'=>'Error',
				'info'=>'Info',
				'success'=>'Success'
			)
		)
	) 
);

// Tooltip
$az_shortcodes['tooltip'] = array( 
	'type'=>'checkbox', 
	'title'=>__('Tooltip', AZ_THEME_NAME), 
	'attr'=>array(
		'mode'=>array(
			'type'=>'radio', 
			'title'=>__('Position', AZ_THEME_NAME), 
			'opt'=>array(
				'top'=>'Top',
				'left'=>'Left',
				'right'=>'Right',
				'bottom'=>'Bottom'
			)
		),
		'text'=>array(
			'type'=>'text', 
			'title'=>__('Text', AZ_THEME_NAME)
		)
	) 
);

// Highlight
$az_shortcodes['highlight'] = array( 
	'type'=>'checkbox', 
	'title'=>__('Highlight', AZ_THEME_NAME), 
	'attr'=>array(
		'mode'=>array(
			'type'=>'radio', 
			'title'=>__('Mode', AZ_THEME_NAME), 
			'opt'=>array(
				'color-text'=>'Color Text',
				'highlight-text'=>'Highlight Text'
			)
		)
	) 
);

// Dropcap
$az_shortcodes['dropcap'] = array( 
	'type'=>'checkbox', 
	'title'=>__('Dropcap', AZ_THEME_NAME), 
	'attr'=>array(
		'mode'=>array(
			'type'=>'radio', 
			'title'=>__('Mode', AZ_THEME_NAME), 
			'opt'=>array(
				'dropcap-normal'=>'Dropcap Normal',
				'dropcap-color'=>'Dropcap Color'
			)
		)
	) 
);

// Lightbox Image
$az_shortcodes['lightbox_image'] = array( 
	'type'=>'regular', 
	'title'=>__('Lightbox Image', AZ_THEME_NAME ), 
	'attr'=>array( 
		'image'=>array('type'=>'custom', 'title'  => __('Image',AZ_THEME_NAME)),
		'title'=>array('type'=>'text', 'title'=>__('Title', AZ_THEME_NAME)),
		'gallery'=>array('type'=>'text', 'title'=>__('Gallery Name', AZ_THEME_NAME))
	)
);

// Lightbox Video
$az_shortcodes['lightbox_video'] = array( 
	'type'=>'regular', 
	'title'=>__('Lightbox Video', AZ_THEME_NAME ), 
	'attr'=>array( 
		'image'=>array('type'=>'custom', 'title'  => __('Image',AZ_THEME_NAME)),
		'title'=>array('type'=>'text', 'title'=>__('Title', AZ_THEME_NAME)),
		'link'=>array('type'=>'text', 'title'=>__('Link URL', AZ_THEME_NAME)),
		'gallery'=>array('type'=>'text', 'title'=>__('Gallery Name', AZ_THEME_NAME))
	)
);

// Information Block
$az_shortcodes['information_block'] = array( 
	'type'=>'checkbox', 
	'title'=>__('Information Block', AZ_THEME_NAME ), 
	'attr'=>array( 
		'url'=>array('type'=>'text', 'title'=>__('Button URL', AZ_THEME_NAME)),
		'button_text'=>array('type'=>'text', 'title'=>__('Button Text', AZ_THEME_NAME))
	)
);

// Button
$az_shortcodes['button'] = array( 
	'type'=>'radios', 
	'title'=>__('Button', AZ_THEME_NAME), 
	'attr'=>array(
		'size'=>array(
			'type'=>'radio', 
			'title'=>__('Size', AZ_THEME_NAME), 
			'opt'=>array(
				'mini'=>'Mini',
				'small'=>'Small',
				'medium'=>'Medium',
				'large'=>'Large'
			)
		),
		'url'=>array(
			'type'=>'text', 
			'title'=>'Link URL'
		),
		'text'=>array(
			'type'=>'text', 
			'title'=>__('Text', AZ_THEME_NAME)
		),
		'invert'=>array(
			'type'=>'checkbox', 
			'title'=>__('Inverted Color?', AZ_THEME_NAME)
		)
	) 
);

// Progress Bar
$az_shortcodes['progress_bar'] = array( 
	'type'=>'regular', 
	'title'=>__('Progress Bar', AZ_THEME_NAME), 
	'attr'=>array( 
		'field'=>array('type'=>'text', 'title'=>__('Field', AZ_THEME_NAME)),
		'percent'=>array('type'=>'text', 'title'=>__('Percentage', AZ_THEME_NAME))
	)
);

// Pricing Table
$az_shortcodes['pricing_table'] = array( 
	'type'=>'checkbox', 
	'title'=>__('Pricing Table', AZ_THEME_NAME ),  
	'attr'=>array( 
		'title'=>array('type'=>'text', 'title'=>__('Title', AZ_THEME_NAME)),
		'price'=>array('type'=>'text', 'title'=>__('Price', AZ_THEME_NAME)), 
		'currency_symbol'=>array('type'=>'text', 'title'=>__('Currency Symbol', AZ_THEME_NAME)),
		'interval'=>array('type'=>'text', 'title'=>__('Interval Time', AZ_THEME_NAME)),
		'url'=>array('type'=>'text', 'title'=>__('Button URL', AZ_THEME_NAME)),
		'text'=>array('type'=>'text', 'title'=>__('Button Text', AZ_THEME_NAME)),
		'highlight'=>array('type'=>'checkbox', 'title'=>__('Highlight Pricing Table?', AZ_THEME_NAME))
	)
);

// Box Services
$az_shortcodes['box_services'] = array( 
	'type'=>'checkbox', 
	'title'=>__('Services Box', AZ_THEME_NAME ),  
	'attr'=>array( 
		'icons' => array(
			'type'=>'icons', 
			'title'=>'Icon', 
			'values'=> array(
			  	'font-icon-zoom-out' 		=> 'font-icon-zoom-out',
			  	'font-icon-zoom-in' 		=> 'font-icon-zoom-in',
			  	'font-icon-wrench'			=> 'font-icon-wrench',
			  	'font-icon-waves'			=> 'font-icon-waves', 
				'font-icon-warning'			=> 'font-icon-warning', 
				'font-icon-volume-up'		=> 'font-icon-volume-up', 
				'font-icon-volume-off'		=> 'font-icon-volume-off', 
				'font-icon-volume-down'		=> 'font-icon-volume-down', 
				'font-icon-viewport'		=> 'font-icon-viewport', 
				'font-icon-user'			=> 'font-icon-user', 
				'font-icon-user-border'		=> 'font-icon-user-border', 
				'font-icon-upload'			=> 'font-icon-upload', 
				'font-icon-upload-2'		=> 'font-icon-upload-2', 
				'font-icon-unlock'			=> 'font-icon-unlock', 
				'font-icon-underline'		=> 'font-icon-underline', 
				'font-icon-tint'			=> 'font-icon-tint', 
				'font-icon-time'			=> 'font-icon-time', 
				'font-icon-text'			=> 'font-icon-text', 
				'font-icon-text-width'		=> 'font-icon-text-width', 
				'font-icon-text-height'		=> 'font-icon-text-height', 
				'font-icon-tags'			=> 'font-icon-tags', 
				'font-icon-tag'				=> 'font-icon-tag', 
				'font-icon-table'			=> 'font-icon-table', 
				'font-icon-strikethrough' 	=> 'font-icon-strikethrough', 
				'font-icon-stop'			=> 'font-icon-stop',
				'font-icon-step-forward'	=> 'font-icon-step-forward', 
				'font-icon-step-backward' 	=> 'font-icon-step-backward', 
				'font-icon-stars'			=> 'font-icon-stars', 
				'font-icon-star'			=> 'font-icon-star', 
				'font-icon-star-line'		=> 'font-icon-star-line', 
				'font-icon-star-half'		=> 'font-icon-star-half', 
				'font-icon-sort'				=> 'font-icon-sort', 
				'font-icon-sort-up'				=> 'font-icon-sort-up', 
				'font-icon-sort-down'			=> 'font-icon-sort-down', 
				'font-icon-sitemap' 			=> 'font-icon-sitemap',
				'font-icon-signout'				=> 'font-icon-signout',
				'font-icon-signin' 				=> 'font-icon-signin',
				'font-icon-signal' 				=> 'font-icon-signal',
				'font-icon-shopping-cart'		=> 'font-icon-shopping-cart', 
				'font-icon-search' 				=> 'font-icon-search',
				'font-icon-rss' 				=> 'font-icon-rss',
				'font-icon-road' 				=> 'font-icon-road',
				'font-icon-retweet' 			=> 'font-icon-retweet',
				'font-icon-resize-vertical'		=> 'font-icon-resize-vertical' ,
				'font-icon-resize-vertical-2' 	=> 'font-icon-resize-vertical-2',
				'font-icon-resize-small' 		=> 'font-icon-resize-small',
				'font-icon-resize-horizontal' 	=> 'font-icon-resize-horizontal',
				'font-icon-resize-horizontal-2' => 'font-icon-resize-horizontal-2',
				'font-icon-resize-fullscreen' 	=> 'font-icon-resize-fullscreen',
				'font-icon-resize-full' 		=> 'font-icon-resize-full',
				'font-icon-repeat' 				=> 'font-icon-repeat',
				'font-icon-reorder' 			=> 'font-icon-reorder',
				'font-icon-remove' 				=> 'font-icon-remove',
				'font-icon-remove-sign'			=> 'font-icon-remove-sign', 
				'font-icon-remove-circle' 		=> 'font-icon-remove-circle',
				'font-icon-read-more' 			=> 'font-icon-read-more',
				'font-icon-random' 				=> 'font-icon-random',
				'font-icon-question-sign'		=> 'font-icon-question-sign', 
				'font-icon-pushpin' 			=> 'font-icon-pushpin',
				'font-icon-pushpin-2'			=> 'font-icon-pushpin-2', 
				'font-icon-print' 				=> 'font-icon-print',
				'font-icon-plus' 				=> 'font-icon-plus',
				'font-icon-plus-sign'			=> 'font-icon-plus-sign', 
				'font-icon-play' 				=> 'font-icon-play',
				'font-icon-picture' 			=> 'font-icon-picture',
				'font-icon-phone' 				=> 'font-icon-phone',
				'font-icon-phone-sign'			=> 'font-icon-phone-sign', 
				'font-icon-phone-boxed' 		=> 'font-icon-phone-boxed',
				'font-icon-pause' 				=> 'font-icon-pause',
				'font-icon-paste' 				=> 'font-icon-paste',
				'font-icon-paper-clip'			=> 'font-icon-paper-clip', 
				'font-icon-ok' 					=> 'font-icon-ok',
				'font-icon-ok-sign'				=> 'font-icon-ok-sign', 
				'font-icon-ok-circle' 			=> 'font-icon-ok-circle',
				'font-icon-music' 				=> 'font-icon-music',
				'font-icon-move' 				=> 'font-icon-move',
				'font-icon-money' 				=> 'font-icon-money',
				'font-icon-minus'				=> 'font-icon-minus',
				'font-icon-minus-sign' 			=> 'font-icon-minus-sign', 
				'font-icon-map' 				=> 'font-icon-map',
				'font-icon-map-marker' 			=> 'font-icon-map-marker' ,
				'font-icon-map-marker-2' 		=> 'font-icon-map-marker-2',
				'font-icon-magnet' 				=> 'font-icon-magnet',
				'font-icon-magic' 				=> 'font-icon-magic',
				'font-icon-lock' 				=> 'font-icon-lock',
				'font-icon-list' 				=> 'font-icon-list',
				'font-icon-list-3' 				=> 'font-icon-list-3',
				'font-icon-list-2' 				=> 'font-icon-list-2',
				'font-icon-link' 				=> 'font-icon-link',
				'font-icon-layer'	 			=> 'font-icon-layer',
				'font-icon-key' 				=> 'font-icon-key',
				'font-icon-italic' 				=> 'font-icon-italic',
				'font-icon-info' 				=> 'font-icon-info',
				'font-icon-indent-right'		=> 'font-icon-indent-right', 
				'font-icon-indent-left' 		=> 'font-icon-indent-left',
				'font-icon-inbox' 				=> 'font-icon-inbox',
				'font-icon-inbox-empty'			=> 'font-icon-inbox-empty', 
				'font-icon-home' 				=> 'font-icon-home',
				'font-icon-heart' 				=> 'font-icon-heart',
				'font-icon-heart-line'			=> 'font-icon-heart-line', 
				'font-icon-headphones' 			=> 'font-icon-headphones',
				'font-icon-headphones-line'		=> 'font-icon-headphones-line', 
				'font-icon-headphones-line-2' 	=> 'font-icon-headphones-line-2',
				'font-icon-headphones-2' 		=> 'font-icon-headphones-2',
				'font-icon-hdd' 				=> 'font-icon-hdd',
				'font-icon-group' 				=> 'font-icon-group',
				'font-icon-grid'				=> 'font-icon-grid',
				'font-icon-grid-large'			=> 'font-icon-grid-large', 
				'font-icon-globe_line' 			=> 'font-icon-globe_line',
				'font-icon-glass' 				=> 'font-icon-glass',
				'font-icon-glass_2' 			=> 'font-icon-glass_2',
				'font-icon-gift' 				=> 'font-icon-gift',
				'font-icon-forward' 			=> 'font-icon-forward',
				'font-icon-font' 				=> 'font-icon-font',
				'font-icon-folder-open'			=> 'font-icon-folder-open', 
				'font-icon-folder-close' 		=> 'font-icon-folder-close',
				'font-icon-flag' 				=> 'font-icon-flag',
				'font-icon-fire' 				=> 'font-icon-fire',
				'font-icon-film' 				=> 'font-icon-film',
				'font-icon-file' 				=> 'font-icon-file',
				'font-icon-file-empty'			=> 'font-icon-file-empty', 
				'font-icon-fast-forward' 		=> 'font-icon-fast-forward',
				'font-icon-fast-backward' 		=> 'font-icon-fast-backward',
				'font-icon-facetime' 			=> 'font-icon-facetime',
				'font-icon-eye' 				=> 'font-icon-eye',
				'font-icon-eye_disable'			=> 'font-icon-eye_disable', 
				'font-icon-expand-view' 		=> 'font-icon-expand-view' ,
				'font-icon-expand-view-3' 		=> 'font-icon-expand-view-3',
				'font-icon-expand-view-2' 		=> 'font-icon-expand-view-2',
				'font-icon-expand-vertical' 	=> 'font-icon-expand-vertical',
				'font-icon-expand-horizontal' 	=> 'font-icon-expand-horizontal',
				'font-icon-exclamation' 		=> 'font-icon-exclamation',
				'font-icon-email' 				=> 'font-icon-email',
				'font-icon-email_2' 			=> 'font-icon-email_2',
				'font-icon-eject' 				=> 'font-icon-eject',
				'font-icon-edit' 				=> 'font-icon-edit',
				'font-icon-edit-check' 			=> 'font-icon-edit-check',
				'font-icon-download' 			=> 'font-icon-download',
				'font-icon-download_2' 			=> 'font-icon-download_2',
				'font-icon-dashboard' 			=> 'font-icon-dashboard',
				'font-icon-credit-card' 		=> 'font-icon-credit-card',
				'font-icon-copy' 				=> 'font-icon-copy',
				'font-icon-comments'	 		=> 'font-icon-comments',
				'font-icon-comments-line'		=> 'font-icon-comments-line', 
				'font-icon-comment'		 		=> 'font-icon-comment' ,
				'font-icon-comment-line'		=> 'font-icon-comment-line', 
				'font-icon-columns' 			=> 'font-icon-columns',
				'font-icon-columns-2' 			=> 'font-icon-columns-2',
				'font-icon-cogs' 				=> 'font-icon-cogs',
				'font-icon-cog' 				=> 'font-icon-cog',
				'font-icon-cloud'				=> 'font-icon-cloud',
				'font-icon-check' 				=> 'font-icon-check',
				'font-icon-check-empty'	 		=> 'font-icon-check-empty', 
				'font-icon-certificate' 		=> 'font-icon-certificate',
				'font-icon-camera' 				=> 'font-icon-camera',
				'font-icon-calendar' 			=> 'font-icon-calendar',
				'font-icon-bullhorn' 			=> 'font-icon-bullhorn',
				'font-icon-briefcase' 			=> 'font-icon-briefcase',
				'font-icon-bookmark' 			=> 'font-icon-bookmark',
				'font-icon-book'	 			=> 'font-icon-book',
				'font-icon-bolt' 				=> 'font-icon-bolt',
				'font-icon-bold'	 			=> 'font-icon-bold' ,
				'font-icon-blockquote' 			=> 'font-icon-blockquote', 
				'font-icon-bell' 				=> 'font-icon-bell' ,
				'font-icon-beaker' 				=> 'font-icon-beaker' ,
				'font-icon-barcode'	 			=> 'font-icon-barcode',
				'font-icon-ban-circle' 			=> 'font-icon-ban-circle',
				'font-icon-ban-chart' 			=> 'font-icon-ban-chart',
				'font-icon-ban-chart-2' 		=> 'font-icon-ban-chart-2',
				'font-icon-backward' 			=> 'font-icon-backward',
				'font-icon-asterisk'	 		=> 'font-icon-asterisk',
				'font-icon-arrow-simple-up' 	=> 'font-icon-arrow-simple-up' , 
				'font-icon-arrow-simple-up-circle' 			=> 'font-icon-arrow-simple-up-circle',
				'font-icon-arrow-simple-right' 				=> 'font-icon-arrow-simple-right',
				'font-icon-arrow-simple-right-circle' 		=> 'font-icon-arrow-simple-right-circle', 
				'font-icon-arrow-simple-left' 				=> 'font-icon-arrow-simple-left',
				'font-icon-arrow-simple-left-circle' 		=> 'font-icon-arrow-simple-left-circle',
				'font-icon-arrow-simple-down' 				=> 'font-icon-arrow-simple-down',
				'font-icon-arrow-simple-down-circle' 		=> 'font-icon-arrow-simple-down-circle', 
				'font-icon-arrow-round-up' 					=> 'font-icon-arrow-round-up',
				'font-icon-arrow-round-up-circle' 			=> 'font-icon-arrow-round-up-circle', 
				'font-icon-arrow-round-right' 				=> 'font-icon-arrow-round-right',
				'font-icon-arrow-round-right-circle' 		=> 'font-icon-arrow-round-right-circle', 
				'font-icon-arrow-round-left' 				=> 'font-icon-arrow-round-left',
				'font-icon-arrow-round-left-circle' 		=> 'font-icon-arrow-round-left-circle', 
				'font-icon-arrow-round-down' 				=> 'font-icon-arrow-round-down',
				'font-icon-arrow-round-down-circle' 		=> 'font-icon-arrow-round-down-circle', 
				'font-icon-arrow-light-up' 					=> 'font-icon-arrow-light-up',
				'font-icon-arrow-light-round-up' 			=> 'font-icon-arrow-light-round-up', 
				'font-icon-arrow-light-round-up-circle' 	=> 'font-icon-arrow-light-round-up-circle', 
				'font-icon-arrow-light-round-right' 		=> 'font-icon-arrow-light-round-right',
				'font-icon-arrow-light-round-right-circle' 	=> 'font-icon-arrow-light-round-right-circle', 
				'font-icon-arrow-light-round-left' 			=> 'font-icon-arrow-light-round-left',
				'font-icon-arrow-light-round-left-circle' 	=> 'font-icon-arrow-light-round-left-circle', 
				'font-icon-arrow-light-round-down' 			=> 'font-icon-arrow-light-round-down',
				'font-icon-arrow-light-round-down-circle' 	=> 'font-icon-arrow-light-round-down-circle' ,
				'font-icon-arrow-light-right' 		=> 'font-icon-arrow-light-right',
				'font-icon-arrow-light-left' 		=> 'font-icon-arrow-light-left',
				'font-icon-arrow-light-down' 		=> 'font-icon-arrow-light-down',
				'font-icon-align-right' 			=> 'font-icon-align-right',
				'font-icon-align-left' 				=> 'font-icon-align-left',
				'font-icon-align-justify' 			=> 'font-icon-align-justify',
				'font-icon-align-center' 			=> 'font-icon-align-center',
				'font-icon-adjust' 					=> 'font-icon-adjust',
				'font-icon-social-zerply' 			=> 'font-icon-social-zerply', 
				'font-icon-social-youtube'  		=> 'font-icon-social-youtube',
				'font-icon-social-yelp' 			=> 'font-icon-social-yelp',
				'font-icon-social-yahoo'			=> 'font-icon-social-yahoo',
				'font-icon-social-wordpress' 		=> 'font-icon-social-wordpress',
				'font-icon-social-virb' 			=> 'font-icon-social-virb',
				'font-icon-social-vimeo' 			=> 'font-icon-social-vimeo',
				'font-icon-social-viddler' 			=> 'font-icon-social-viddler',
				'font-icon-social-twitter' 			=> 'font-icon-social-twitter',
				'font-icon-social-tumblr' 			=> 'font-icon-social-tumblr',
				'font-icon-social-stumbleupon'		=> 'font-icon-social-stumbleupon',
				'font-icon-social-soundcloud' 		=> 'font-icon-social-soundcloud',
				'font-icon-social-skype' 			=> 'font-icon-social-skype',
				'font-icon-social-share-this'		=> 'font-icon-social-share-this',
				'font-icon-social-quora' 			=> 'font-icon-social-quora',
				'font-icon-social-pinterest'		=> 'font-icon-social-pinterest',
				'font-icon-social-photobucket'		=> 'font-icon-social-photobucket',
				'font-icon-social-paypal' 			=> 'font-icon-social-paypal',
				'font-icon-social-myspace' 			=> 'font-icon-social-myspace',
				'font-icon-social-linkedin' 		=> 'font-icon-social-linkedin',
				'font-icon-social-last-fm' 			=> 'font-icon-social-last-fm',
				'font-icon-social-instagram'		=> 'font-icon-social-instagram',
				'font-icon-social-grooveshark' 		=> 'font-icon-social-grooveshark',
				'font-icon-social-google-plus' 		=> 'font-icon-social-google-plus',
				'font-icon-social-github' 			=> 'font-icon-social-github',
				'font-icon-social-forrst' 			=> 'font-icon-social-forrst',
				'font-icon-social-flickr' 			=> 'font-icon-social-flickr',
				'font-icon-social-facebook' 		=> 'font-icon-social-facebook',
				'font-icon-social-evernote' 		=> 'font-icon-social-evernote',
				'font-icon-social-envato' 			=> 'font-icon-social-envato',
				'font-icon-social-email' 			=> 'font-icon-social-email', 
				'font-icon-social-dribbble'			=> 'font-icon-social-dribbble',
				'font-icon-social-digg' 			=> 'font-icon-social-digg',
				'font-icon-social-deviant-art' 		=> 'font-icon-social-deviant-art',
				'font-icon-social-blogger' 			=> 'font-icon-social-blogger',
				'font-icon-social-behance'			=> 'font-icon-social-behance',
				'font-icon-social-bebo' 			=> 'font-icon-social-bebo',
				'font-icon-social-addthis'			=> 'font-icon-social-addthis',
				'font-icon-social-500px' 			=> 'font-icon-social-500px'
			)
		),
		'title'=>array('type'=>'text', 'title'=>__('Title', AZ_THEME_NAME)),
		'url'=>array('type'=>'text', 'title'=>__('Optional URL', AZ_THEME_NAME))
	)
);

// Revolution Slider
$az_shortcodes['slider'] = array(  
	'type'=>'regular', 
	'title'=>__('Revolution Slider', AZ_THEME_NAME ),
	'attr'=>array(
		'alias'=>array('type'=>'text', 'title'=>__('Revolution Slider Alias', AZ_THEME_NAME)) 
	)
);

// Video Embed Code
$az_shortcodes['video_embed'] = array(  
	'type'=>'checkbox', 
	'title'=>__('Video Embed Code', AZ_THEME_NAME ),
	'attr'=>array(
		'class'=>array('type'=>'text', 'title'=>__('Class', AZ_THEME_NAME)) 
	)
);

// Video
$az_shortcodes['video'] = array( 
	'type'=>'regular', 
	'title'=>__('Video', AZ_THEME_NAME ), 
	'attr'=>array( 
		'm4v_url'=>array('type'=>'text', 'title'=>__('M4V File URL', AZ_THEME_NAME)),
		'ogv_url'=>array('type'=>'text', 'title'=>__('OGV FILE URL', AZ_THEME_NAME)),
		'image'=>array('type'=>'custom', 'title'=> __('Preview Image',AZ_THEME_NAME))
	)
);

// Audio
$az_shortcodes['audio'] = array( 
	'type'=>'regular', 
	'title'=>__('Audio', AZ_THEME_NAME ), 
	'attr'=>array( 
		'mp3_url'=>array('type'=>'text', 'title'=>__('MP3 File URL', AZ_THEME_NAME)),
		'ogg_url'=>array('type'=>'text', 'title'=>__('OGG File URL', AZ_THEME_NAME)),
		'image'=>array('type'=>'custom', 'title'=> __('Preview Image',AZ_THEME_NAME), 'desc'=> __('The preview image should be the same dimensions as your video.',AZ_THEME_NAME))
	)
);


?>