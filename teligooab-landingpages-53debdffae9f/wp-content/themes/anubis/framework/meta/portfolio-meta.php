<?php

/*-----------------------------------------------------------------------------------*/
/*	Portfolio Meta
/*-----------------------------------------------------------------------------------*/


add_action('add_meta_boxes', 'az_metabox_portfolio');
function az_metabox_portfolio(){
    
	
/*-----------------------------------------------------------------------------------*/
/*	Thumbnails Setting
/*-----------------------------------------------------------------------------------*/

	$meta_box = array(
		'id' => 'az-metabox-portfolio-settings',
		'title' =>  __('Portfolio Thumbnails Settings', AZ_THEME_NAME),
		'description' => __('You can choose if you want use a fancybox gallery or insert a pop-up video.', AZ_THEME_NAME),
		'post_type' => 'portfolio',
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
    		array( 
				'name' => __('FancyBox Gallery', AZ_THEME_NAME),
				'desc' => __('Please enter gallery name.', AZ_THEME_NAME),
				'id' => '_az_fancy_gallery',
				'type' => 'text',
				'std' => ''
			),
			array( 
				'name' => __('FancyBox Video', AZ_THEME_NAME),
				'desc' => __('Please enter Video URL.', AZ_THEME_NAME),
				'id' => '_az_fancy_video',
				'type' => 'text',
				'std' => ''
			)
		)
	);
	
	$callback = create_function( '$post,$meta_box', 'az_create_meta_box( $post, $meta_box["args"] );' );
	add_meta_box( $meta_box['id'], $meta_box['title'], $callback, $meta_box['post_type'], $meta_box['context'], $meta_box['priority'], $meta_box );
	
	
/*-----------------------------------------------------------------------------------*/
/*	Header Settings
/*-----------------------------------------------------------------------------------*/

    $meta_box = array(
		'id' => 'az-metabox-portfolio-header',
		'title' => __('Portfolio Header Settings', AZ_THEME_NAME),
		'description' => __('Here you can configure how your page header will appear. <br/> For a full width background image behind your header text, simply upload the image below. To have a standard header just fill out the fields below and don\'t upload an image.', AZ_THEME_NAME),
		'post_type' => 'portfolio',
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array( 
					'name' => __('Page Header Image', AZ_THEME_NAME),
					'desc' => __('Upload your image should be between 1600px x 800px (or more) for best results.', AZ_THEME_NAME),
					'id' => '_az_portfolio_header_bg',
					'type' => 'file',
					'std' => ''
				),
			array( 
					'name' => __('Revolution Slider Alias', AZ_THEME_NAME),
					'desc' => __('Enter the revolution slider alias for the slider that you want to show.', AZ_THEME_NAME),
					'id' => '_az_portfolio_intro_slider_header',
					'type' => 'text',
					'std' => ''
				)
		)
	);
	
	add_meta_box( $meta_box['id'], $meta_box['title'], $callback, $meta_box['post_type'], $meta_box['context'], $meta_box['priority'], $meta_box );

}