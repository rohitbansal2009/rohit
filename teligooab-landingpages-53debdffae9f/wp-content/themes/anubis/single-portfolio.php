<?php get_header(); 

$options = get_option('anubis'); 

?>

<?php if( !empty($options['enable-big-featured-image']) && $options['enable-big-featured-image'] == 1) { ?>

<?php 			
	$image_header = get_post_meta($post->ID, '_az_portfolio_header_bg', true);
	$rev_slider_alias = get_post_meta($post->ID, '_az_portfolio_intro_slider_header', true);
?>

<?php    

	// Custom Image Header 
	if( !empty($image_header)) { ?>
	<section id="image-static">
        <div class="fullimage-container">
            <div class="pattern"></div>
            <img src="<?php echo $image_header; ?>" alt="<?php the_title(); ?>" />
        </div>
    </section>
<?php }

	// Slider Header
	else if( !empty($rev_slider_alias)) { ?>
	<section id="slider-header">
		<?php echo do_shortcode('[rev_slider '.$rev_slider_alias.']'); ?>
    </section>
<?php } 
	
	// Featured Image Header
	else { ?>
	<section id="image-static">
        <div class="fullimage-container">
            <div class="pattern"></div>
            <?php echo get_the_post_thumbnail($post->ID, 'full', array('title' => '')); ?>
        </div>
    </section>
<?php } ?>

<?php } ?>

<?php
	// Get Project Attributes
	$attrs = get_the_terms( $post->ID, 'project-attribute' );
	$attributes_fields = NULL;
	
	if ( !empty($attrs) ){
	 foreach ( $attrs as $attr ) {
	   $attributes_fields[] = $attr->name;
	 }
	 
	 $on_attributes = join( ", ", $attributes_fields );
	}
	
	// Add Specific Class to Intro Box
	$featured_header = $options['enable-big-featured-image'];

	switch ($featured_header) {
		case 1 :
			$intro_class = 'single-post featured-post';
			break;
		case 0 :
			$intro_class = 'single-post no-featured-post';
			break;
	}
?>

<section id="intro-box" class="<?php echo $intro_class; ?>">
	<div class="container">
    	<div class="row">
        	<div class="span12">
            	<h3><?php the_title(); ?></h3>
                <h4><?php echo $on_attributes; ?></h4>
                <span class="arrow"></span>
            </div>
        </div>
    </div>
</section>

<div id="content">
	<div class="container">
    	<section id="portfolio-content">      
			<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
				<?php edit_post_link( __('Edit', AZ_THEME_NAME), '<span class="edit-post">[', ']</span>' ); ?>
                <?php the_content(); ?>
            <?php endwhile; endif; ?>
        </section>
        
        <?php $portfolio_link = get_portfolio_page_link(get_the_ID()); ?>
            
        <div class="row">
            <div class="span12">
                <div class="paginate-portfolio">
                    <div class="separator"></div>
                    <ul>
                        <li class="prev"><?php previous_post_link('%link', '<i class="font-icon-arrow-simple-left"></i>%title'); ?></li>
                        <li class="back-page"><a href="<?php echo $portfolio_link; ?>"><i class="font-icon-grid"></i><?php _e('Back to Portfolio', AZ_THEME_NAME); ?></a></li> 
                        <li class="next"><?php next_post_link('%link', '<i class="font-icon-arrow-simple-right"></i>%title'); ?></li> 
                    </ul>
                </div>
            </div>
        </div>
        
    </div>
</div>
	
<?php get_footer(); ?>