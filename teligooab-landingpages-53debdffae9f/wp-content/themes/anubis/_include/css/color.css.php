<?php header("Content-type: text/css; charset=utf-8"); 

$absolute_path = __FILE__;
$path_to_file = explode( 'wp-content', $absolute_path );
$path_to_wp = $path_to_file[0];

require_once( $path_to_wp . '/wp-load.php' );

$options = get_option('anubis'); 
?>

a:hover,
a:active,
a:focus,
header #logo a:hover,
nav#menu #menu-nav li a:hover,
nav#menu #menu-nav li.sfHover a,
nav#menu #menu-nav li.active a,
nav#menu #menu-nav li.current_page_item a,
nav#menu #menu-nav li.current-page-ancestor a,
#navigation-mobile li a:hover,
.tp-caption.custom_text_link a,
#single-team .team-social ul li a:hover i,
.box:hover .icon i,
.box:active .icon i,
.box.tapped .icon i,
.box:hover h4,
.box:active h4,
.box.tapped h4,
.blog-post-full .entry-title a:hover,
.blog-post-full .entry-meta a:hover,
.single-post .entry-title a:hover,
.single-post .entry-meta a:hover,
.comment-author cite a:hover,
.comment-meta a:hover,
#commentform span.required,
#latest-blog .entry-title a:hover,
#latest-blog .entry-meta a:hover,
#twitter-feed .tweet_list li .tweet_text a,
#twitter-feed .tweet_list li .tweet_time a,
#twitter-feed .tweet_list li .tweet_time,
#footer-credits p a:hover,
.footer-widgets a,
.dropmenu-active ul li a:hover,
.color-text {
	color: <?php echo $options['accent-color']; ?>;
}


nav#menu #menu-nav ul,
.tp-bullets.simplebullets.round .bullet:hover,
.tp-bullets.simplebullets.round .bullet.selected,
.tparrows:hover,
#intro-box,
.single-people .team-img .overlay,
#single-team .paginate-team ul li.prev a:hover,
#single-team .paginate-team ul li.next a:hover,
#single-team .paginate-team ul li.back-page a:hover,
#single-team .paginate-team .separator,
.item-project .hover-wrap .overlay,
.paginate-portfolio ul li.prev a:hover,
.paginate-portfolio ul li.next a:hover,
.paginate-portfolio ul li.back-page a:hover,
.paginate-portfolio .separator,
.wpcf7 .wpcf7-submit:hover,
.blog-post-full .post-thumb .overlay,
.blog-post-full .post .separator,
.blog-post-full .entry-type a,
.blog-post-full .pagination-blog ul li.prev a:hover,
.blog-post-full .pagination-blog ul li.next a:hover,
#intro-box.single-post,
.single-post .post .separator,
.single-post .post-thumb .overlay,
.badge,
#commentform #submit:hover,
.work-item-thumbs .hover-wrap .overlay,
#latest-blog .post .separator,
#latest-blog .post-thumb .overlay,
#social-footer ul li:hover a,
#back-to-top:hover,
.jp-play-bar,
.jp-volume-bar-value,
.tagcloud a,
.widget_quick-flickr-widget ul li a .overlay,
.twitter-widget .twitter-link a,
.social_widget a:hover,
.footer-widgets .social_widget a:hover,
.button-main:hover,
.button-main:active,
.button-main:focus,
.button-main.inverted,
.dropmenu:hover,
.dropmenu:hover,
.dropmenu.open,
.dropmenu.open,
.accordion-heading:hover .accordion-toggle,
.accordion-heading:hover .accordion-toggle.inactive,
.accordion-heading .accordion-toggle.active,
.nav > li > a:hover,
.nav > li > a:focus,
.nav-tabs > .active > a,
.nav-tabs > .active > a:hover,
.nav-tabs > .active > a:focus,
.tooltip-inner,
.highlight-text,
.dropcap.color,
.progress .bar,
.lightbox .hover-wrap .overlay-img,
.pricing-table.selected h5,
.pricing-table.selected .price,
.pricing-table.selected .button-main.confirm,
.icons-example ul li:hover a,
.icons-example ul li.active a,
.show-grid:hover,
.fancybox-nav:hover span {
	background-color: <?php echo $options['accent-color']; ?>;
}



.wpcf7-form.invalid input.wpcf7-not-valid,
.wpcf7-form.invalid textarea.wpcf7-not-valid,
.wpcf7-form input:focus:invalid:focus,
.wpcf7-form textarea:focus:invalid:focus,
.info-block {
	border-color: <?php echo $options['accent-color']; ?>;
}

nav#menu #menu-nav li:hover ul:before {
	border-bottom-color: <?php echo $options['accent-color']; ?>;
}

.tooltip.top .tooltip-arrow {
  	border-top-color: <?php echo $options['accent-color']; ?>;
}

.tooltip.right .tooltip-arrow {
  	border-right-color: <?php echo $options['accent-color']; ?>;
}

.tooltip.left .tooltip-arrow {
  	border-left-color: <?php echo $options['accent-color']; ?>;
}

.tooltip.bottom .tooltip-arrow {
  	border-bottom-color: <?php echo $options['accent-color']; ?>;
}


@media (min-width: 320px) and (max-width: 979px) {
	#sidebar,
	#contact .span3 {
    	border-color: <?php echo $options['accent-color']; ?>;
	}
}


















