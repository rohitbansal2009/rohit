<?php 
/* Template Name: Team */

get_header(); 

$options = get_option('anubis'); 

$cols = (!empty($options['main_team_layout'])) ? $options['main_team_layout'] : '4' ;

switch ($cols) {
	case '4' :
		$span_num = '3';
		break;
	case '3' :
		$span_num = '4';
		break;
	case '2' :
		$span_num = '6';
		break;
}
?>

<?php az_page_header($post->ID); ?>

<div id="content">
	<div class="container">
    
    	<section id="team-filter" class="margin-50">
        	<div class="row">
            	<div class="span12">
                
                	<div class="dropdown">
                        <div class="dropmenu">
                            <p class="selected"><?php _e('All Discipline', AZ_THEME_NAME); ?></p>
                            <i class="font-icon-arrow-simple-down"></i>
                        </div>
            
                        <div class="dropmenu-active">
                            <ul class="option-set" data-option-key="filter">
                                <li><a class="selected drop-selected" href="#filter" data-option-value="*"><?php _e('All Discipline', AZ_THEME_NAME); ?></a></li>
                                
                                <?php $list_disciplines = get_categories("taxonomy=disciplines");
									foreach ($list_disciplines as $list_discipline) : 									
									echo '<li><a href="#filter" data-option-value=".' . strtolower(str_replace(" ","-", ($list_discipline->name))) . '">' . $list_discipline->name . '</a></li>';
									endforeach;        
								?>

                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        
        <section id="team">
        	<div class="row">
            	<div class="span12">
                	<div id="team-people" class="row">
                    	<ul id="people">
                        	
                            <?php
								$posts_per_page = '-1';
								$team = array(
									'posts_per_page' => $posts_per_page,
									'post_type' => 'team',
									'orderby' => 'menu_order',
									'order' => 'ASC'
								);
		
								$wp_query = new WP_Query($team);
							
								if(have_posts()) : while(have_posts()) : the_post(); ?>
                                
                                <?php 
				
									$terms = get_the_terms($post->id,"disciplines");
									$discipline_cats = NULL;
									
									if ( !empty($terms) ){
									 foreach ( $terms as $term ) {
									   $discipline_cats .= strtolower($term->slug) . ' ';
									 }
									}
									   
									
									$attrs = get_the_terms( $post->ID, 'attributes' );
									$attributes_fields = NULL;
									
									if ( !empty($attrs) ){
									 foreach ( $attrs as $attr ) {
									   $attributes_fields[] = $attr->name;
									 }
									 
									 $on_attributes = join( " - ", $attributes_fields );
									}
								
								?>
                               
                                <li class="single-people span<?php echo $span_num; ?> <?php echo $discipline_cats; ?>">
                                    <a class="team-hover" href="<?php echo get_post_permalink($id); ?>">
                                        <div class="team-img">
                                        	<?php the_post_thumbnail(); ?>
                                            <div class="overlay"></div>
                                            <i class="font-icon-group"></i>
                                        </div>
                                        <div class="team-name">
                                        	<h3><?php the_title(); ?></h3>
                                            <h4><?php echo $on_attributes; ?></h4>
                                        </div>
                                    </a>
                                </li>
                                
                                <?php endwhile; endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        
    </div>
</div>

<?php get_footer(); ?>