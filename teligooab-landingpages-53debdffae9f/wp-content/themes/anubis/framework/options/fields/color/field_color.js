jQuery(document).ready(function ($) {
    $('input.popup-colorpicker').wpColorPicker({
    	palettes: [
			'#1ABC9C', 
			'#2ECC71', 
			'#3498DB', 
			'#9B59B6', 
			'#F39C12', 
			'#E74C3C', 
			'#F97E76'
		]
    });
});
