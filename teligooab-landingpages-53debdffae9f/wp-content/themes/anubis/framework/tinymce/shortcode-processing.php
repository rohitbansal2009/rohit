<?php

/*-----------------------------------------------------------------------------------*/
/*	Theme Grid Columns - Reference Bootstrap Framework
/*-----------------------------------------------------------------------------------*/

// Row
function az_row( $atts, $content = null ) {
    extract(shortcode_atts(array("class" => ''), $atts));
	
	if($class)
	$class = ' '.esc_attr($class);
	
	return '<div class="row'.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('row', 'az_row');

// 1/12 Column
function az_span1( $atts, $content = null ) {
    extract(shortcode_atts(array("class" => ''), $atts));
	
	if($class)
	$class = ' '.esc_attr($class);
	
	return '<div class="span1'.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('span1', 'az_span1');

// 2/12 Column
function az_span2( $atts, $content = null ) {
    extract(shortcode_atts(array("class" => ''), $atts));
	
	if($class)
	$class = ' '.esc_attr($class);
	
	return '<div class="span2'.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('span2', 'az_span2');

// 3/12 Column
function az_span3( $atts, $content = null ) {
    extract(shortcode_atts(array("class" => ''), $atts));
	
	if($class)
	$class = ' '.esc_attr($class);
	
	return '<div class="span3'.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('span3', 'az_span3');

// 4/12 Column
function az_span4( $atts, $content = null ) {
    extract(shortcode_atts(array("class" => ''), $atts));
	
	if($class)
	$class = ' '.esc_attr($class);
	
	return '<div class="span4'.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('span4', 'az_span4');

// 5/12 Column
function az_span5( $atts, $content = null ) {
    extract(shortcode_atts(array("class" => ''), $atts));
	
	if($class)
	$class = ' '.esc_attr($class);
	
	return '<div class="span5'.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('span5', 'az_span5');

// 6/12 Column
function az_span6( $atts, $content = null ) {
    extract(shortcode_atts(array("class" => ''), $atts));
	
	if($class)
	$class = ' '.esc_attr($class);
	
	return '<div class="span6'.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('span6', 'az_span6');

// 7/12 Column
function az_span7( $atts, $content = null ) {
    extract(shortcode_atts(array("class" => ''), $atts));
	
	if($class)
	$class = ' '.esc_attr($class);
	
	return '<div class="span7'.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('span7', 'az_span7');

// 8/12 Column
function az_span8( $atts, $content = null ) {
    extract(shortcode_atts(array("class" => ''), $atts));
	
	if($class)
	$class = ' '.esc_attr($class);
	
	return '<div class="span8'.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('span8', 'az_span8');

// 9/12 Column
function az_span9( $atts, $content = null ) {
    extract(shortcode_atts(array("class" => ''), $atts));
	
	if($class)
	$class = ' '.esc_attr($class);
	
	return '<div class="span9'.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('span9', 'az_span9');

// 10/12 Column
function az_span10( $atts, $content = null ) {
    extract(shortcode_atts(array("class" => ''), $atts));
	
	if($class)
	$class = ' '.esc_attr($class);
	
	return '<div class="span10'.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('span10', 'az_span10');

// 11/12 Column
function az_span11( $atts, $content = null ) {
    extract(shortcode_atts(array("class" => ''), $atts));
	
	if($class)
	$class = ' '.esc_attr($class);
	
	return '<div class="span11'.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('span11', 'az_span11');

// 12/12 Column
function az_span12( $atts, $content = null ) {
    extract(shortcode_atts(array("class" => ''), $atts));
	
	if($class)
	$class = ' '.esc_attr($class);
	
	return '<div class="span12'.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('span12', 'az_span12');


/*-----------------------------------------------------------------------------------*/
/*	Elements
/*-----------------------------------------------------------------------------------*/

// Page Title
function az_page_title( $atts, $content = null ) {
    extract(shortcode_atts(array("class" => ''), $atts));
	
	if($class)
	$class = ' '.esc_attr($class);
	
	return'
	<div class="row'.$class.'">
		<div class="span12">
			<div class="page-title">
				<h3>'.$content.'</h3>
			</div>
		</div>
	</div>';
}
add_shortcode('page_title', 'az_page_title');

// Divider
function az_divider($atts, $content = null) {  
    extract(shortcode_atts(array("class" => ''), $atts));
	
	if($class)
	$class = ' '.esc_attr($class);
	
	return '<div class="divider'.$class.'"></div>';
}
add_shortcode('divider', 'az_divider');

// Accordion
function az_accordions($atts, $content = null){
	$GLOBALS['accordion_count'] = 0;
	do_shortcode( $content );
	
	if( is_array( $GLOBALS['accordions'] ) ){
		
		foreach( $GLOBALS['accordions'] as $accordion ){
			$accordions[] = '<div class="accordion-group">
							<div class="accordion-heading accordionize">
								<a class="accordion-toggle" href="#'.$accordion['id'].'" data-parent="#accordionArea" data-toggle="collapse">'.$accordion['title'].'<span class="font-icon-arrow-simple-down"></span></a>
							</div>
							<div id="'.$accordion['id'].'" class="accordion-body collapse">
								<div class="accordion-inner">'.$accordion['content'].'</div>
							</div>
						</div>';
		}
		
		$return = '<div id="accordionArea" class="accordion">'.implode( "\n", $accordions )."</div>";
	}
	
	return $return;
}

add_shortcode('accordion_section', 'az_accordions');

function az_accordion($atts, $content){
	extract(shortcode_atts(array( 'title' => '%d', 'id' => '%d'), $atts));
	
	$x = $GLOBALS['accordion_count'];
	$GLOBALS['accordions'][$x] = array(
		'title' => sprintf( $title, $GLOBALS['accordion_count'] ),
		'content' =>  do_shortcode($content),
		'id' =>  $id );
	
	$GLOBALS['accordion_count']++;
}

add_shortcode( 'accordion', 'az_accordion' );

// Toggle
function az_toggles($atts, $content = null){
	$GLOBALS['toggle_count'] = 0;
	do_shortcode( $content );
	
	if( is_array( $GLOBALS['toggles'] ) ){
		
		foreach( $GLOBALS['toggles'] as $toggle ){
			$toggles[] = '<div class="accordion-group">
							<div class="accordion-heading togglize">
								<a class="accordion-toggle" href="#'.$toggle['id'].'" data-parent="#" data-toggle="collapse">'.$toggle['title'].'<span class="font-icon-plus"></span><span class="font-icon-minus"></span></a>
							</div>
							<div id="'.$toggle['id'].'" class="accordion-body collapse">
								<div class="accordion-inner">'.$toggle['content'].'</div>
							</div>
						</div>';
		}
		
		$return = '<div id="toggleArea" class="accordion">'.implode( "\n", $toggles )."</div>";
	}
	
	return $return;
}

add_shortcode('toggle_section', 'az_toggles');

function az_toggle($atts, $content){
	extract(shortcode_atts(array( 'title' => '%d', 'id' => '%d'), $atts));
	
	$x = $GLOBALS['toggle_count'];
	$GLOBALS['toggles'][$x] = array(
		'title' => sprintf( $title, $GLOBALS['toggle_count'] ),
		'content' =>  do_shortcode($content),
		'id' =>  $id );
	
	$GLOBALS['toggle_count']++;
}

add_shortcode( 'toggle', 'az_toggle' );

// Tabs
function az_tabs($atts, $content = null){
	$GLOBALS['tab_count'] = 0;
	do_shortcode( $content );
	
	$id = rand();
	$id = $id*rand(0,100);
	
	
	if( is_array( $GLOBALS['tabs'] ) ){
		
		foreach( $GLOBALS['tabs'] as $tab ){
			$tabs[] = '<li><a href="#'.$tab['id'].'" data-toggle="tab">'.$tab['title'].'</a></li>';
			$panels [] = '<div id="'.$tab['id'].'" class="tab-pane fade in">'.$tab['content'].'</div>';
			
		}
		
		$return = '<div class="tabbable"><ul id="myTab-'.$id.'" class="nav nav-tabs">'.implode( "\n", $tabs ).'</ul>
						<div class="tab-content">
							'.implode( "\n", $panels ).'
						</div>
					
					</div>';
	}
	
	return $return;
}

add_shortcode('tab_section', 'az_tabs');

function az_tab($atts, $content){
	extract(shortcode_atts(array( 'title' => '%d', 'id' => '%d'), $atts));
	
	$x = $GLOBALS['tab_count'];
	$GLOBALS['tabs'][$x] = array(
		'title' => sprintf( $title, $GLOBALS['tab_count'] ),
		'content' =>  do_shortcode($content),
		'id' =>  $id );
	
	$GLOBALS['tab_count']++;
}

add_shortcode( 'tab', 'az_tab' );

// Alert Boxs
function az_alert($atts, $content = null) {  
    extract(shortcode_atts(array("mode" => 'standard', "class" => ''), $atts));
	
	if($class)
	$class = ' '.esc_attr($class);
	
	switch ($mode) {
		case 'standard' :
			$alert_mode = '<div class="alert alert-standard fade in'.$class.'"><a class="close" href="#" data-dismiss="alert">×</a>'. do_shortcode($content) .'</div>';
			break;
		case 'warning' :
			$alert_mode = '<div class="alert fade in'.$class.'"><a class="close" href="#" data-dismiss="alert">×</a>'. do_shortcode($content) .'</div>';
			break;
		case 'error' :
			$alert_mode = '<div class="alert alert-error fade in'.$class.'"><a class="close" href="#" data-dismiss="alert">×</a>'. do_shortcode($content) .'</div>';
			break;
		case 'info' :
			$alert_mode = '<div class="alert alert-info fade in'.$class.'"><a class="close" href="#" data-dismiss="alert">×</a>'. do_shortcode($content) .'</div>';
			break;
		case 'success' :
			$alert_mode = '<div class="alert alert-success fade in'.$class.'"><a class="close" href="#" data-dismiss="alert">×</a>'. do_shortcode($content) .'</div>';
			break;	
	}
    return $alert_mode;
}
add_shortcode('alert_box', 'az_alert');

// Tootltip
function az_tooltip($atts, $content = null) {  
    extract(shortcode_atts(array("mode" => 'top', "text" => 'Tooltip'), $atts));
	
	switch ($mode) {
		case 'top' :
			$tooltip_mode = '<a href="#" data-toggle="tooltip" data-original-title="'.$text.'" data-placement="top">'. do_shortcode($content) .'</a>';
			break;
		case 'left' :
			$tooltip_mode = '<a href="#" data-toggle="tooltip" data-original-title="'.$text.'" data-placement="left">'. do_shortcode($content) .'</a>';
			break;
		case 'right' :
			$tooltip_mode = '<a href="#" data-toggle="tooltip" data-original-title="'.$text.'" data-placement="right">'. do_shortcode($content) .'</a>';
			break;
		case 'bottom' :
			$tooltip_mode = '<a href="#" data-toggle="tooltip" data-original-title="'.$text.'" data-placement="bottom">'. do_shortcode($content) .'</a>';
			break;
	}
    return $tooltip_mode;
}
add_shortcode('tooltip', 'az_tooltip');

// Highlights
function az_highlight($atts, $content = null) {  
    extract(shortcode_atts(array("mode" => 'color-text'), $atts));
	
	switch ($mode) {
		case 'color-text' :
			$highlight_mode = '<span class="color-text">'. do_shortcode($content) .'</span>';
			break;
		case 'highlight-text' :
			$highlight_mode = '<span class="highlight-text">'. do_shortcode($content) .'</span>';
			break;
	}
    return $highlight_mode;
}
add_shortcode('highlight', 'az_highlight');

// DropCaps
function az_dropcap($atts, $content = null) {  
    extract(shortcode_atts(array("mode" => 'dropcap-normal'), $atts));
	
	switch ($mode) {
		case 'dropcap-normal' :
			$dropcap_mode = '<p><span class="dropcap">'. do_shortcode($content) .'</span>';
			break;
		case 'dropcap-color' :
			$dropcap_mode = '<p><span class="dropcap color">'. do_shortcode($content) .'</span>';
			break;
	}
    return $dropcap_mode;
}
add_shortcode('dropcap', 'az_dropcap');

// Lightbox Image
function az_lightbox_image($atts, $content = null) {  
    extract(shortcode_atts(array('image_url' => '', 'title' => '', 'gallery' => ''), $atts));
		
	$html = null;
			
	$html .= '<div class="lightbox">';
	$html .= '<a class="hover-wrap fancybox" title="' . $title . '" href="' . $image_url .'" data-fancybox-group="'. $gallery .'">';
	$html .= '<span class="overlay-img"></span><span class="overlay-img-thumb font-icon-search"></span>';
	$html .= '</a>';
	$html .= '<img alt="'.$title.'" src="' . $image_url .'" />';
	$html .= '</div>';
	
	return $html;
	 
}
add_shortcode('lightbox_image', 'az_lightbox_image');

// Lightbox Video
function az_lightbox_video($atts, $content = null) {  
    extract(shortcode_atts(array('image_url' => '', 'title' => '', 'link' => '', 'gallery' => ''), $atts));
		
	$html = null;
			
	$html .= '<div class="lightbox">';
	$html .= '<a class="hover-wrap fancybox-media" title="' . $title . '" href="' . $link .'" data-fancybox-group="'. $gallery .'">';
	$html .= '<span class="overlay-img"></span><span class="overlay-img-thumb font-icon-play"></span>';
	$html .= '</a>';
	$html .= '<img alt="'.$title.'" src="' . $image_url .'" />';
	$html .= '</div>';
	
	return $html;
	 
}
add_shortcode('lightbox_video', 'az_lightbox_video');

// Information Block
function az_information_block( $atts, $content = null ) {
    extract(shortcode_atts(array('url' => '#', 'button_text' => 'Purchase'), $atts));
	
	return'
	<div class="info-block">
		<div class="info-text">
			'. do_shortcode($content) .'
			<a class="button-main button-large" target="_blank" href="'.$url.'">'.$button_text.'</a>
		</div>
	</div>';
}
add_shortcode('information_block', 'az_information_block');

// Button
function az_button($atts, $content = null) {  
    extract(shortcode_atts(array("size" => 'medium', "url" => '#', "text" => 'Button Text', "invert" => 'false'), $atts));
	switch ($size) {
		case 'mini' :
			($invert == 'true') ? $button_size = '<a class="button-main inverted button-mini"' :  $button_size = '<a class="button-main button-mini"';
			break;
		case 'small' :
			($invert == 'true') ? $button_size = '<a class="button-main inverted button-small"' :  $button_size = '<a class="button-main button-small"';
			break;
		case 'medium' :
			($invert == 'true') ? $button_size = '<a class="button-main inverted"' :  $button_size = '<a class="button-main"';
			break;
		case 'large' :
			($invert == 'true') ? $button_size = '<a class="button-main inverted button-large"' :  $button_size = '<a class="button-main button-large"';
			break;	
	}
    return $button_size . ' href="' . $url . '">' . $text . '</a>';
}
add_shortcode('button', 'az_button');

// Progress Bar
function az_progress_bar($atts, $content = null) {  
	extract(shortcode_atts(array("field" => '', "percent" => ''), $atts));
	
	return '<div class="progress-bar">
				<div class="progress">
					<span class="field">'.$field.'</span>
					<div class="bar" style="width: '.$percent.'%"></div>
				</div>
			</div>';
}
add_shortcode('progress_bar', 'az_progress_bar');

// Pricing Tables
function az_pricing_table($atts, $content = null) {
	extract(shortcode_atts(array("title"=>'Column title', "highlight" => 'false', "price" => "99", "currency_symbol" => '$', "interval" => 'Per Month', "url" => "#", "text" => "Purchase Now"), $atts));	
	$highlight_class = null;
	
	if($highlight == 'true') {
		$highlight_class = 'selected'; 
	}
	
    return '<div class="pricing-table '.$highlight_class.'">
  				<h5>'.$title.'</h5>
				<div class="price">
					'.$currency_symbol.$price.'<span>'.$interval.'</span>
				</div>
            ' . do_shortcode($content) . '
			<a class="button-main confirm" href="'.$url.'">'.$text.'</a>
			</div>';
}
add_shortcode('pricing_table', 'az_pricing_table');

// Box Services
function az_box_services($atts, $content = null) {
	extract(shortcode_atts(array("image"=>"", "title"=>"Box", "url" => "#"), $atts));	
	
    return '<a class="box" href="'.$url.'">
				<div class="icon">
					<i class="'.$image.'"></i>
				</div>
				<h4>'.$title.'</h4>
				<p>' . do_shortcode($content) . '</p>
			</a>';
}
add_shortcode('box_services', 'az_box_services');


// Revolution Slider
function az_slider( $atts, $content = null ) {
    extract(shortcode_atts(array("alias" => ''), $atts));
	
	return do_shortcode('[rev_slider '.$alias.']');
}
add_shortcode('slider', 'az_slider');


// Video Embed Code
function az_video_embed( $atts, $content = null ) {
    extract(shortcode_atts(array("class" => ''), $atts));
	
	if($class)
	$class = ' '.esc_attr($class);
	
	return '<div class="video-embed'.$class.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('video_embed', 'az_video_embed');


// Video
function az_shortcode_video($atts, $content = null) {
	extract(shortcode_atts(array("title" => 'Title', 'm4v_url' => null, 'ogv_url' => null, 'image_url' => null), $atts));  
	$video_markup = null;
		
		$id = rand();
		$id = $id*rand(1,50);
	
		$video_markup .= '<script type="text/javascript">
			jQuery(document).ready(function($){
			
				if( $().jPlayer ) {
					$("#jquery_jplayer_'.$id.'").jPlayer({
						ready: function () {
							$(this).jPlayer("setMedia", {
								m4v: "'.$m4v_url.'",
								ogv: "'. $ogv_url .'",
								poster: "'. $image_url .'"
							});
						},
						size: {
						  cssClass: "jp-jplayer-video",
						  width: "100%",
						  height: "auto"
						},
						swfPath: "'. get_template_directory_uri() .'/_include/js",
						cssSelectorAncestor: "#jp_interface_'.$id.'",
						supplied: "m4v, ogv"
					});
				}
			});
		</script>
	
		<div id="jquery_jplayer_'.$id.'" class="jp-jplayer jp-jplayer-video"></div>
	
		<div class="jp-video-container">
			<div class="jp-video">
				<div id="jp_interface_'.$id.'" class="jp-interface">
					<ul class="jp-controls">
						<li><div class="seperator-first"></div></li>
						<li><div class="seperator-second"></div></li>
						<li><a href="#" class="jp-play" tabindex="1">play</a></li>
						<li><a href="#" class="jp-pause" tabindex="1">pause</a></li> 
						<li><a href="#" class="jp-mute" tabindex="1">mute</a></li>
						<li><a href="#" class="jp-unmute" tabindex="1">unmute</a></li>
					</ul>
					<div class="jp-progress">
						<div class="jp-seek-bar">
							<div class="jp-play-bar"></div>
						</div>
					</div>
					<div class="jp-volume-bar-container">
						<div class="jp-volume-bar">
							<div class="jp-volume-bar-value"></div>
						</div>
					</div>
				</div>
	
			</div>
		</div>';
	
    return $video_markup;
	
}

add_shortcode('video', 'az_shortcode_video');

// Audio
function az_shortcode_audio($atts, $content = null) {
	extract(shortcode_atts(array("title" => 'Title', 'mp3_url' => null, 'ogg_url' => null, 'image_url' => null), $atts));  
	$audio_markup = null;
	
	$id = rand();
	$id = $id*rand(1,50);
	
	$audio_markup .= '<script type="text/javascript">
		
    			jQuery(document).ready(function($){
	
    				if( $().jPlayer ) {
    					$("#jquery_jplayer_'.$id.'").jPlayer({
    						ready: function () {
    							$(this).jPlayer("setMedia", {
    								mp3: "'.$mp3_url.'",
    								oga: "'.$ogg_url.'",
									poster: "'. $image_url .'",
									end: ""
    							});
    						},
							size: {
							  width: "100%",
							  height: "auto"
							},
    						swfPath: "'. get_template_directory_uri().' /_include/js",
    						cssSelectorAncestor: "#jp_interface_'.$id.'",
    						supplied: "oga, mp3"
    					});
					
    				}
    			});
    		</script>
			
			<div class="audio-wrap">
				
	    	    <div id="jquery_jplayer_'.$id.'" class="jp-jplayer jp-jplayer-audio"></div>
	
	            <div class="jp-audio-container">
	                <div class="jp-audio">
	                    <div id="jp_interface_'.$id.'" class="jp-interface">
	                        <ul class="jp-controls">
	                            <li><a href="#" class="jp-play" tabindex="1">play</a></li>
	                            <li><a href="#" class="jp-pause" tabindex="1">pause</a></li>
	                            <li><a href="#" class="jp-mute" tabindex="1">mute</a></li>
	                            <li><a href="#" class="jp-unmute" tabindex="1">unmute</a></li>
	                        </ul>
	                        <div class="jp-progress">
	                            <div class="jp-seek-bar">
	                                <div class="jp-play-bar"></div>
	                            </div>
	                        </div>
	                        <div class="jp-volume-bar-container">
	                            <div class="jp-volume-bar">
	                                <div class="jp-volume-bar-value"></div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
            </div>';
	
    return $audio_markup;
}

add_shortcode('audio', 'az_shortcode_audio');

?>