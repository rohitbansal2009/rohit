<?php $link_url = get_post_meta($post->ID, '_az_link', true); ?>

<?php if( !is_single() ) { ?>

<div class="entry-type">
    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><i class="font-icon-link"></i></a>
</div>

<div class="row">
	<div class="span6 offset3">
    	<h2 class="entry-title link-text"><a href="<?php echo $link_url; ?>" title="<?php the_title(); ?>"> <?php the_title(); ?></a></h2>        
    </div>
</div>

<?php } else { ?>

<h2 class="entry-title link-text text-align-center"><a href="<?php echo $link_url; ?>" title="<?php the_title(); ?>"> <?php the_title(); ?></a></h2>
<div class="separator"></div>

<div class="entry-content">
    <?php the_content( __("Continue Reading...", AZ_THEME_NAME) );?>
</div>

<?php get_template_part( 'content' , 'meta-footer' ); ?>

<?php } ?>